<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <script src="webcam.min.js"></script>

	<div id="my_camera" style="width:320px; height:240px;"></div>
    <input type="text" id="nameOfImage" />
	<div id="my_result"></div>

	<script language="JavaScript">
		
        function cameraOn(){
            Webcam.set({
                width: 320,
                height: 240,
                crop_width: 240,
                crop_height: 240
	        });
            Webcam.attach( '#my_camera' );
        }
		function take_snapshot() {
			Webcam.snap( function(data_uri) {
                var username = 'jhuckaby';
                var image_fmt = 'jpeg';
                var url = 'myscript.php?username=' + username + '&format=' + image_fmt;
				document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';

                Webcam.upload( data_uri, url, function(code, text) {
                    // Upload complete!
                    // 'code' will be the HTTP response code from the server, e.g. 200
                    // 'text' will be the raw response content
                });
			});

            Webcam.reset();
		}
	</script>
    <a href="javascript:void(cameraOn())">Camera ON</a>
    <br>
	<a href="javascript:void(take_snapshot())">Take Snapshot</a>
</body>
</html>