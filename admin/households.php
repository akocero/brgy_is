<?php
    include "session_admin.php";
    include_once("../conn.php");
    include 'templates/tmp_header.php';
?>
    

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Households</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Data Entry</a></li>
                                <li class="breadcrumb-item active">Households</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">

                <div id="modal" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add/Update category</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row" id="editContent">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Family Name</label>
                                            <input type="text" class="form-control" placeholder="Family Name.." id="text_name" val="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Details</label>
                                            <input type="text" class="form-control" placeholder="Details ..." id="text_details" val="">
                                        </div>
                                    </div>
                                    <input type="hidden" id="editRowID" value="0">
                                </div>
                                <div id="householdsMembers">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Resident Name</label> 
                                                    <select class="form-control select2" style="width: 100%;" id="h_member_id">
                                                        <option value="">...</option>
                                                        <?php
                                                            $sql = $conn->query("SELECT id, concat(last_name, ' ', first_name) as name From residents");
                                                            if($sql->num_rows > 0) {
                                                                while ($data = $sql->fetch_array()) {
                                                                    echo '<option value="'.$data["id"].'">'.$data["name"].'</option>';
                                                                }
                                                            }
                                                        ?>                                                       
                                                    </select>         
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Relation to Head</label>
                                                <select id="h_relation" class="form-control select2" style="width: 100%;">
                                                    <option value="">...</option>
                                                    <option value="head">Head of Family</option>
                                                    <option value="grand father">Grand Father</option>
                                                    <option value="grand mother">Grand Mother</option>
                                                    <option value="wife">Wife</option>
                                                    <option value="husband">Husband</option>
                                                    <option value="father">Father</option>
                                                    <option value="mother">Mother</option>
                                                    <option value="son">Son</option>
                                                    <option value="daughter">Daughter</option>
                                                    <option value="brother">Brother</option>
                                                    <option value="syster">Sister</option>
                                                    <option value="girlfriend">Girlfriend</option>
                                                    <option value="cousin">Cousin</option>
                                                    <option value="uncle">Uncle</option>
                                                    <option value="untie">Untie</option>
                                                    <option value="father in law">Father in Law</option>
                                                    <option value="mother in law">Mother in Law</option>
                                                    <option value="other">Others ...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Household Status</label>
                                                <select id="h_status" class="form-control">
                                                    <option value="">...</option>
                                                    <option value="head">Head</option>
                                                    <option value="member">Member</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <p class="lead input-title text-uppercase">Houshold Members</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <div class="hm-table-container">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btn_manage_data" value="Save" onclick="manageData('addNew')" class="btn btn-success btn-block btn-flat">
                                <input type="button" id="btn_add_members" value="Add Household Member" onclick="addHousehold()" class="btn btn-success btn-block btn-flat">
                                <input type="button" id="btn_close_modal" style="display: none;" class="btn btn-default btn-block btn-flat" data-dismiss="modal" value="Close">
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <input type="button" id="addNew" class="btn btn-primary btn-flat btn-sm" value="+ Add Households">
                        
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <table class="table table-sm table-hover table-bordered mainTable">
                                <thead style="font-weight: 500;">
                                    <tr>
                                        <td>ID</td>
                                        <td>Full Name</td>
                                        <td>Active</td>
                                        <td>Created On</td>
                                        <td>Tools</td>
                                    </tr>
                                </thead>
                                <tbody class="mainTable-body">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'templates/tmp_footer.php' ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#addNew").on('click', function() {
                $('.modal-title').html('Add new household');
                $("#text_name").val('');
                $("#text_details").val('');
                $('#btn_close_modal').hide();
                $('#btn_manage_data').val('Save').attr('onclick', 'manageData("addNew")').show();
                $("#modal").modal('show');
                $('#editContent').show();
                $('#householdsMembers').hide();
                $('#btn_add_members').hide();
            });
            getExistingData(0, 10);
            $('.select2').select2();


        });

        function viewORedit(rowID, type) {
            $.ajax({
                url: 'functions/ajax_households.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'getRowData',
                    rowID: rowID
                },
                success: function(response) {
                    if (type == "view") {

                        $("#text_name").val(response.name);
                        $("#text_details").val(response.details);
                        $('#btn_close_modal').show();
                        $('#btn_manage_data').hide();
                    } else {
                        $("#text_name").val(response.name);
                        $("#text_details").val(response.details);
                        $('#btn_close_modal').hide();
                        $('#btn_manage_data').val('Save changes').attr('onclick', 'manageData("updateRow")');
                        $('#btn_manage_data').show();

                    }
                    $('.modal-title').html(response.name);
                    $('#editContent').show();
                    $('#householdsMembers').hide();
                    $('#btn_add_members').hide();
                    $('#editRowID').val(rowID);
                    $("#modal").modal('show');
                }
            });

        }

        function resetHh(){
            $('#h_member_id').val('');
            $('#h_status').val('');
            $('#h_relation').val('');
        }

        function addHousehold(){
            var h_member_id = $('#h_member_id');
            var h_status = $('#h_status');
            var h_relation = $('#h_relation');
            var editRowID = $("#editRowID");

            if (isNotEmpty(h_member_id) && isNotEmpty(h_relation) && isNotEmpty(h_status) && isNotEmpty(editRowID)) {
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: 'addHouseholdMember',
                        h_member_id: h_member_id.val(),
                        h_status: h_status.val(),
                        h_relation: h_relation.val(),
                        rowID: editRowID.val()
                    },
                    success: function(response) {
                      if(response == '1 head per household'){
                        toastr.error(response);
                      }else if (response == 'Data already added'){
                        toastr.error(response);
                      }else if(response == 'Data added'){
                        addedRowMember();
                        toastr.success(response);
                        resetHh();
                      }else{
                        toastr.error(response);
                      }
                        
                    }
                });
            }
            
        }

        function deleteMember(hm_id){
            if(confirm('This data will be deleted from the household')){
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'deleteRowMember',
                        hm_id: hm_id
                    }, success: function (response){
                        if(response == 'Data Deleted'){
                            $("#mem_" + hm_id).remove();
                            toastr.success(response);
                        }else{
                            toastr.error(response);
                        }
                        
                    }
                });
            }else{
                toastr.info('Good Decision');
            }
            
        }

        function householdsMembers(h_id, h_name) {
            $('.modal-title').html(h_name);
            $('.hm-table-container').html('');
            $('#editRowID').val(h_id);
            $.ajax({
                url: 'functions/ajax_households.php',
                method: 'POST',
                dataType: 'text',
                data: {
                    key: 'getHouseholdsMembers',
                    h_id: h_id,
                    addedQry: ''
                },
                success: function(response) {
                    if (response != '0'){
                        $('.hm-table-container').html('<table class="table table-bordered table-sm hm-table"><thead><tr><td>Name</td><td>Relation</td><td>Status</td><td>Created On</td><td>Tools</td></tr></thead><tbody class="hm-tbody"></tbody></table>');
                        $('.hm-tbody').html(response);
                        $("table.hm-table").DataTable({
                                "order": [[ 0, "desc" ]]
                            });
                        
                    }else{
                        $('.hm-table-container').html('<table class="table table-bordered table-sm hm-table"><thead><tr><td>Name</td><td>Relation</td><td>Status</td><td>Created On</td><td>Tools</td></tr></thead><tbody class="hm-tbody"><tr><td colspan="5">No Members Found!</td></tr></tbody></table>');
                    }
                    
                }
            });
            $('#editContent').hide();
            $('#btn_manage_data').hide();
            $('#btn_close_modal').hide();
            $('#householdsMembers').show();
            $('#btn_add_members').show();
            $("#modal").modal('show');
        }

        function updateRowStatus(id, status){
                var statusLbl = "";
                var statusBtn = "";
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'updateRowStatus',
                        status: status,
                        rowID: id
                    }, success: function (response){
                        if(response == "Active updated"){
                            if(status == 'active'){
                                status = 'inactive';
                                statusLbl = "Inactive";
                                statusBtn = "danger";
                            }else{
                                status = 'active';
                                statusLbl = "Active";
                                statusBtn = "success";
                            }
                           $("#status_"+id).html("<button onclick='updateRowStatus("+id+",\""+status+"\")' class='btn btn-"+statusBtn+" btn-flat btn-sm' type='button'>"+statusLbl+"</button>"); 
                        }else{
                            alert(response);
                        }
                    }
                });
            }

        function addedRow(){
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'addRow',
                    }, success: function (response){
                        $(".mainTable-body tr:first").before(response);
                    }
                });
            }

        function addedRowMember(){
                var h_id = $("#editRowID");
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'getHouseholdsMembers',
                        addedQry: 'limit 1',
                        h_id: h_id.val()
                    }, success: function (response){
                        var firstRow = $(".hm-tbody tr:first");
                        if(firstRow.html() == '<td colspan="5">No Members Found!</td>'){
                            $(".hm-tbody").html(response);
                        }else if($(".hm-tbody").html().indexOf('tr') == -1){
                            $(".hm-tbody").append(response);
                        }else {
                           firstRow.before(response);
                           // 
                        }
                        
                    }
                });
            }

        function getExistingData(start, limit) {
            $.ajax({
                url: 'functions/ajax_households.php',
                method: 'POST',
                dataType: 'text',
                data: {
                    key: 'getExistingData',
                    start: start,
                    limit: limit
                },
                success: function(response) {
                    if (response != "reachedMax") {
                        $('.mainTable-body').append(response);
                        start += limit;
                        getExistingData(start, limit);
                    } else {
                        $(".mainTable").DataTable({
                                "order": [[ 0, "desc" ]]
                            });
                    }
                }
            });
        }

        function manageData(key) {
            var name = $("#text_name");
            var details = $("#text_details");
            var editRowID = $("#editRowID");

            if (isNotEmpty(name) && isNotEmpty(details)) {
                $.ajax({
                    url: 'functions/ajax_households.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: key,
                        name: name.val(),
                        details: details.val(),
                        rowID: editRowID.val()
                    },
                    success: function(response) {
                        if (response == "Data added") {
                            toastr.success(response);
                            addedRow();
                        } else if (response == "Data updated") {
                            toastr.info(response);
                            $("#name_" + editRowID.val()).html(name.val());
                        } else {
                            toastr.error(response);
                        }
                        $("#modal").modal('hide');
                        $("#btn_manage_data").attr('value', 'Save').attr('onclick', "manageData('addNew')");
                    }
                });
            }
        }

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.addClass('is-invalid');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.removeClass('is-invalid');
                return true;
            }
        }
    </script>
</body>

</html>