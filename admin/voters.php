<?php
    include "session_admin.php";
    include_once("../conn.php");
    include 'templates/tmp_header.php';
?>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link active">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Registered Voters</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item active">Registered Voters</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div id="modalRoom" class="modal fade">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="room-modal-title">Enter Number of Room</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" id="text_room" class="form-control" placeholder="Ex. 30">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="printVoters" class="btn btn-secondary btn-flat btn-block"><i class="fa fa-print"></i> Print Voters</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="modal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add/Update category</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row" id="editContent">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" placeholder="First Name..." id="text_first_name" val="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name..." id="text_last_name" val="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Middle Name</label>
                                            <input type="text" class="form-control" placeholder="Username..." id="text_middle_name" val="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Voter ID</label>
                                            <input type="text" id="text_voter_id" class="form-control" placeholder="Password" val="" />
                                        </div>
                                    </div>
                                    <input type="hidden" id="editRowID" value="0">
                                </div>
                            </div>
                        

                            <div class="modal-footer">
                                <input type="button" id="btn_manage_data" value="save" onclick="manageData('addNew')" class="btn btn-success btn-block btn-flat">
                                <input type="button" id="btn_close_modal" style="display: none;" class="btn btn-default btn-block btn-flat" data-dismiss="modal" value="Close">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <button id="openModalRoom" class="btn btn-primary btn-flat btn-sm" target="_blank">Print Voters</button>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <table class="table table-sm table-hover table-bordered">
                                <thead style="font-weight: 500;">
                                    <tr>
                                        <td>ID</td>
                                        <td>Full Name</td>
                                        <td>Active</td>
                                        <td>Created On</td>
                                        <td>Tools</td>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include 'templates/tmp_footer.php'; ?>

    <script type="text/javascript">
        $(document).ready(function() {
            getExistingData(0, 10);

            $('#openModalRoom').on('click', function() {
                $("#modalRoom").modal('show');
            });

            $('#printVoters').on('click', function() {
                printVoters();
            });

            $("#text_room").inputFilter(function(value) {
                return /^\d*$/.test(value);    // Allow digits only, using a RegExp
            });
        });

        function printVoters() {
            const room = $('#text_room');
            if(isNotEmpty(room)){
                window.open('print_voters.php?r=' + room.val(),'_blank');
                room.val('');
                $("#modalRoom").modal('hide');
            }
        }

        function viewORedit(rowID, type) {
            $.ajax({
                url: 'functions/ajax_voters.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'getRowData',
                    rowID: rowID
                },
                success: function(response) {
                    if (type == "view") {

                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_middle_name").val(response.middle_name);
                        $("#text_voter_id").val(response.voter_id);

                        $('#btn_close_modal').show();
                        $('#btn_manage_data').hide();
                    } else {
                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_middle_name").val(response.middle_name);
                        $("#text_voter_id").val(response.voter_id);

                        $('#btn_close_modal').hide();
                        $('#btn_manage_data').val('Save changes').attr('onclick', 'manageData("updateRow")');
                        $('#btn_manage_data').show();

                    }
                    $('.modal-title').html(response.last_name + ', ' + response.first_name + ' ' + response.middle_name + '.')
                    $('#editRowID').val(rowID);
                    $("#modal").modal('show');
                }
            });

        }

        function updateRowStatus(id, status){
                var statusLbl = "";
                var statusBtn = "";
                $.ajax({
                    url: 'functions/ajax_voters.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'updateRowStatus',
                        status: status,
                        rowID: id
                    }, success: function (response){
                        if(response == "Active updated"){
                            if(status == 'active'){
                                status = 'inactive';
                                statusLbl = "Inactive";
                                statusBtn = "danger";
                            }else{
                                status = 'active';
                                statusLbl = "Active";
                                statusBtn = "success";
                            }
                           $("#status_"+id).html("<button onclick='updateRowStatus("+id+",\""+status+"\")' class='btn btn-"+statusBtn+" btn-flat btn-sm' type='button'>"+statusLbl+"</button>"); 
                        }else{
                            alert(response);
                        }
                    }
                });
            }

        function addedRow(){
                $.ajax({
                    url: 'functions/ajax_voters.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'addRow',
                    }, success: function (response){
                        $("tbody tr:first").before(response);
                    }
                });
            }

        function getExistingData(start, limit) {
            $.ajax({
                url: 'functions/ajax_voters.php',
                method: 'POST',
                dataType: 'text',
                data: {
                    key: 'getExistingData',
                    start: start,
                    limit: limit
                },
                success: function(response) {
                    if (response != "reachedMax") {
                        $('tbody').append(response);
                        start += limit;
                        getExistingData(start, limit);
                    } else {
                        $(".table").DataTable({
                                "order": [[ 1, "asc" ]]
                            });
                    }
                }
            });
        }

        function manageData(key) {
            var voter_id = $("#text_voter_id");
            var editRowID = $("#editRowID");

            if (isNotEmpty(voter_id)) {
                $.ajax({
                    url: 'functions/ajax_voters.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: key,
                        voter_id: voter_id.val(),
                        rowID: editRowID.val()
                    },
                    success: function(response) {
                        if (response == "Data updated") {
                            toastr.success(response);
                        } else {
                            toastr.error(response);
                        }
                        $("#modal").modal('hide');
                        $("#btn_manage_data").attr('value', 'Save').attr('onclick', "manageData('addNew')");
                    }
                });
            }
        }

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.addClass('is-invalid');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.removeClass('is-invalid');
                return true;
            }
        }
    </script>
</body>

</html>