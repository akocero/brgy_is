<?php
include "session_admin.php";
include_once("../conn.php");
include 'templates/tmp_print_header.php';
date_default_timezone_set('Asia/Manila');
$dateNow = date("d/m/Y");
$dateOrdinal = date("D, jS \of M Y");

$name = $_GET["name"];
$loc = $_GET["loc"];
$op = $_GET["op"];
$adr = $_GET["adr"];

	function genOfficial($conn, $position){
		$sql = $conn->query("SELECT full_name From officials where position = '$position'");
		if ($sql->num_rows > 0) {
		$data = $sql->fetch_array();
		$officialName = $data["full_name"];
		return $officialName;
		}
	}
?>
<div class="certificate-wrapper mt-2">
	<img src="../Images/logo.png" class="watermark" alt="" width="100%" >
	<div class="header">
		<img src="../Images/marilaoLogo2.png" alt="">
		<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
		<img src="../Images/logo.png" alt="">
	</div>
	<br>
	<div class="line">
		<h4>OFFICE OF THE SANGGUNIANG BARANGAY</h4>
	</div>
	<div class="report-content">
		<div class="row">
			<div class="col-md-3 certOfficials">
				<p class="my-4 lg-letter">Council Members</p>
				
				<?php
					$sql= $conn->query("SELECT full_name From officials Where position = 'councilor'");
					if($sql->num_rows > 0){
						while($data = $sql->fetch_array()){
				?>
						<p class="text-uppercase my-2 sm-letter t-bold"><?php echo $data["full_name"]; ?></p>
				<?php
						}
					}
				?>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Secretary</p>
				<p class="text-uppercase sm-letter mb-0 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Treasurer</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Administrator</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Clerk</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Record Keeper</p>
			</div>
			<div class="col-md-9 certLetters">
				<h1 class="text-center mt-4">BARANGAY BUSINESS CLEARANCE</h1>
				<h4 class="text-center">This is to certify that the business or trade activity described below:</h4>
				<br>
				<h3 class="text-center my-0"><?php echo $name ?></h3>
				<hr class="my-1 lineUnder">
				<h4 class="text-center">(Business / Trade Name)</h4>
				<br>
				<h3 class="text-center my-0"><?php echo $loc ?></h3>
				<hr class="my-1 lineUnder">
				<h4 class="text-center">(Location)</h4>
				<br>
				<h3 class="text-center my-0"><?php echo $op ?></h3>
				<hr class="my-1 lineUnder">
				<h4 class="text-center">(Operator / Manager)</h4>
				<br>
				<h3 class="text-center my-0"><?php echo $adr ?></h3>
				<hr class="my-1 lineUnder">
				<h4 class="text-center">(Address)</h4>
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proposed to be established in this Barangay and is being applied for a Barangay Business Clearance to be used is securing a corresponding Municipal Permit has been found to be in conformity with the provisions of existing Barangay ordinances, rules and regulation being enforced in this Barangay.</h4>
				<br>
				<h4>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In view of the foregning, the undersigned interposes no Objections for issuance of the corresponding Municipal Permit being applied for.
				</h4>
				<br>
				<div class="col-md-8">
					<h4>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Issued this <span><?php echo $dateOrdinal; ?></span> at Sta Rosa 1, Marilao. Bulacan.
					</h4>
				</div>
				
				
				<div class="row">
					<div class="col-md-4 text-left dryseal">
						<br><br><br><br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<p>This Barangay Clearance is not valid without dry seal.</p>
						
					</div>
				</div>	
				<div class="chairmanContainer">
						<img src="../Images/kapPic.png" alt="">
						<div class="chairmanNameB">
							<img src="../Images/kapsig.png" alt="">
							<h1 class="text-uppercase j"><?php echo genOfficial($conn, 'chairman'); ?></h1>
							<h2 class="text-uppercase text-center">Barangay Captain</h2>
						</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- Bootstrap -->
<script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>