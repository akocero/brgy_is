<?php
include "session_admin.php";
include_once("../conn.php");
include 'templates/tmp_print_header.php';
date_default_timezone_set('Asia/Manila');
$dateNow = date("d/m/Y");
$dateOrdinal = date("D, jS \of M Y");

$id = $_GET["id"];
$issuedFor = $_GET["issuedFor"];
$age = $_GET["age"];
$sql = $conn->query("SELECT first_name, middle_name, last_name, special, status, citizenship, address From residents where id = '$id'");
if ($sql->num_rows > 0) {
	$data = $sql->fetch_array();
    $first_name = $data["first_name"];
    $last_name = $data["last_name"];
	$middle_name = $data["middle_name"];
	$special = $data["special"];
	$address = $data["address"];
	$citizenship = $data["citizenship"];
	$status = $data["status"];
}

function genOfficial($conn, $position){
	$sql = $conn->query("SELECT full_name From officials where position = '$position'");
	if ($sql->num_rows > 0) {
	$data = $sql->fetch_array();
	$officialName = $data["full_name"];
	return $officialName;
}

}
?>
<div class="certificate-wrapper mt-2">
	<img src="../Images/logo.png" class="watermark" alt="" width="100%" >
	<div class="header">
		<img src="../Images/marilaoLogo2.png" alt="">
		<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
		<img src="../Images/logo.png" alt="">
	</div>
	<br>
	<div class="line">
		<h4>OFFICE OF THE SANGGUNIANG BARANGAY</h4>
	</div>
	<div class="report-content">
		<div class="row">
			<div class="col-md-3 certOfficials">
				<p class="my-4 lg-letter">Council Members</p>
				
				<?php
					$sql= $conn->query("SELECT full_name From officials Where position = 'councilor'");
					if($sql->num_rows > 0){
						while($data = $sql->fetch_array()){
				?>
						<p class="text-uppercase my-2 sm-letter t-bold"><?php echo $data["full_name"]; ?></p>
				<?php
						}
					}
				?>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Secretary</p>
				<p class="text-uppercase sm-letter mb-0 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Treasurer</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Administrator</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Clerk</p>
				<p class="text-uppercase sm-letter mb-0 mt-5 t-bold"><?php echo genOfficial($conn, 'sk chairman'); ?></p>
				<p class="sm-letter">Barangay Record Keeper</p>
			</div>
			<div class="col-md-9 certLetters">
				<h1 class="text-center my-5">BARANGAY CLEARANCE</h1>
				<h4>TO WHOM IT MAY CONCERN:</h4>
				<br>
				<p class="certLetter">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is to certify that <span class="t-boldUnderline"><?php echo ucfirst($first_name) . ' ' . ucfirst($middle_name) . ' ' . ucfirst($last_name) . ' ' . ucfirst($special); ?></span>.  of legal age <span class="t-boldUnderline">(<?php echo $age; ?>)</span> years old, <span class="t-boldUnderline"><?php echo ucfirst($status); ?></span>, <span class="t-boldUnderline"><?php echo ucfirst($citizenship); ?></span>, and bona fide resident of <span class="t-boldUnderline"><?php echo ucfirst($address); ?></span>, Barangay Sta. Rosa 1, Marilao Bulacan is personally known to me to be a person of good moral character and reputation.</p>
				<br>
				<p class="certLetter">He / She is a peaceful and law abiding citizen.</p>
				<br>
				<p class="certLetter">This Certification is issued upon request of interested party <span class="t-boldUnderline">Party Name</span> in connection with his/her application for <span class="t-boldUnderline"><?php echo ucfirst($issuedFor); ?></span><br>Given this day of <span class="t-boldUnderline"><?php echo ucfirst($dateOrdinal); ?></span></p>
				<br>
				<br>
				<div class="row">
					<div class="col-md-12">
					<h4>_________________________</h4>
					<h4>Signature Over Printed Name</h4>
					</div>
					
					<div class="col-md-5 text-left dryseal">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
						<p>This Barangay Clearance is not valid without dry seal.</p>
					</div>
					<div class="col-md-12">
					<br>



						<h4>CTC No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</h4>
						<h4>Issued at  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Brgy Sta Rosa 1</h4>
						<h4>Date issued &nbsp;: <?php echo $dateNow ?></h4>
						
					</div>
					<div class="chairmanContainer">
						<img src="../Images/kapPic.png" alt="">
						<div class="chairmanName">
							<img src="../Images/kapsig.png" alt="">
							<h1 class="text-uppercase j"><?php echo genOfficial($conn, 'chairman'); ?></h1>
							<h2 class="text-uppercase text-center">Barangay Captain</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Bootstrap -->
<script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>