<?php
include "session_admin.php";
include_once("../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Ymd");
$yearNow = date("Y");
$teenage = $yearNow - 17;
$adult = $yearNow - 19;
$tweties = $yearNow - 29;
$thirties = $yearNow - 39;
$fourties = $yearNow - 49;
$fifthies = $yearNow - 59;
$senior = $yearNow - 69;
$oldAges = $yearNow - 110;
$twoDaysAgo = $dateNow - 2;

    function countActiveOrInactiveOf($table, $identifier, $active, $conn, $qry){
        $sql = $conn->query("SELECT count(id) as total from $table where $identifier = '$active'" . $qry);
        $data = $sql->fetch_array();
        return $data["total"];
    }

    function residentCountBy($identifier, $value, $conn){
        $sql = $conn->query("SELECT count(id) as total from residents where $identifier = '$value' and active = 'active'");
        $data = $sql->fetch_array();
        return $data["total"];
    }

    function residentCountByAge($identifier, $yearNow, $conn){
        $sql = $conn->query("SELECT count(id) as total FROM residents WHERE date_of_birth BETWEEN CAST(concat('$identifier','-01-01') AS DATE) AND CAST(concat('$yearNow','-12-30') AS DATE)");
        $data = $sql->fetch_array();
        return $data["total"];
    }

    function percentage($identifier, $gender, $conn){
        $totalGender = residentCountBy($identifier, $gender, $conn);
        $activeResident = countActiveOrInactiveOf('residents', 'active', 'active', $conn,'');
        $total = ($totalGender / $activeResident) * 100; 
        $total = round($total);
        return "$totalGender ($total%)";
    }

    function newResidents($conn, $twoDaysAgo){
        $sql = $conn->query("SELECT id, Date_format(created_at, '%Y%m%d') as created_at from residents where active = 'active' and created_at >= $twoDaysAgo");
        return $sql->num_rows;
    }

    include 'templates/tmp_header.php';
?>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index.php" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Summary</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Summary</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Active Residents</span>
                                <span class="info-box-number">
                                    <?php echo countActiveOrInactiveOf('residents','active','active',$conn, ''); ?>
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-times"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Inactive Residents</span>
                                <?php  echo countActiveOrInactiveOf('residents','active','inactive',$conn,''); ?>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-child"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Households (Active)</span>
                                <?php echo countActiveOrInactiveOf('households','active','active',$conn,''); ?>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-plus"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">New Residents (2 Days)</span>
                                <span class="info-box-number"><?php echo newResidents($conn, $twoDaysAgo); ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- Default box -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Gender (Acitve)</h3>
                            </div>
                            <div class="card-body">  
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="chart-responsive">
                                            <div class="chartjs-size-monitor">
                                            <div class="chartjs-size-monitor-expand">
                                                <div class="">

                                                </div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink">
                                                <div class="">

                                                </div>
                                            </div>
                                        </div>
                                        <canvas id="donutGender" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 351px;" width="351" height="250" class="chartjs-render-monitor"></canvas>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="chart-legend clearfix">
                                            <li class="lead"><i class="fas fa-mars text-primary"></i> Male</li>
                                            <li class="lead"><i class="fas fa-venus text-pink mr-1"></i> Female</li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer bg-white p-0">
                                <ul class="nav nav-pills flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    Total Population
                                    <span class="float-right text-success">
                                        
                                        <?php echo countActiveOrInactiveOf('residents', 'active', 'active', $conn,''); ?> (100%)</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    <i class="fas fa-mars"></i>  Male
                                    <span class="float-right text-primary">
                                        
                                        <?php echo percentage('gender','male', $conn); ?>
                                    </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    <i class="fas fa-venus mr-1"></i>  Female
                                    <span class="float-right text-pink">
                                        
                                        <?php echo percentage('gender','female', $conn); ?>
                                    </span>
                                    </a>
                                </li>
                                </ul>
                            </div>
                        <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Voters (Active)</h3>
                            </div>
                            <div class="card-body">  
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="chart-responsive">
                                            <div class="chartjs-size-monitor">
                                            <div class="chartjs-size-monitor-expand">
                                                <div class="">

                                                </div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink">
                                                <div class="">

                                                </div>
                                            </div>
                                        </div>
                                        <canvas id="donutVoters" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 351px;" width="351" height="250" class="chartjs-render-monitor"></canvas>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer bg-white p-0">
                                <ul class="nav nav-pills flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    Total Population
                                    <span class="float-right text-primary">
                                        
                                        <?php echo countActiveOrInactiveOf('residents', 'active', 'active', $conn,''); ?> (100%)</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    <i class="fas fa-registered"></i> 
                                    Registered Voters
                                    <span class="float-right text-success">
                                        <?php echo percentage('registered_voter','yes', $conn); ?>
                                    </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                    <i class="fas fa-registered"></i> 
                                    Not Registered Voters
                                    <span class="float-right text-danger">
                                        <?php echo percentage('registered_voter','no', $conn); ?>
                                    </span>
                                    </a>
                                </li>
                                </ul>
                            </div>
                        <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info-box mb-3 bg-primary">
                            <span class="info-box-icon"><i class="fas fa-mars"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Male Resident (Active)</span>
                                <span class="info-box-number"><?php echo percentage('gender','male', $conn); ?></span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                        <div class="info-box mb-3 bg-pink">
                            <span class="info-box-icon"><i class="fas fa-venus"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Female Resident (Active)</span>
                                <span class="info-box-number"><?php echo percentage('gender','female', $conn); ?></span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>

                        <div class="info-box mb-3 bg-success">
                            <span class="info-box-icon"><i class="fas fa-registered"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Registered Voters (Active)</span>
                                <span class="info-box-number"><?php echo percentage('registered_voter','yes', $conn); ?></span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                        <div class="info-box mb-3 bg-danger">
                            <span class="info-box-icon"><i class="fas fa-registered"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Not Registered Voters (Active)</span>
                                <span class="info-box-number"><?php echo percentage('registered_voter','no', $conn); ?></span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                        <div class="info-box mb-3 bg-info">
                            <span class="info-box-icon"><i class="fa fa-fw fa-battery-full"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Population (Active)</span>
                                <span class="info-box-number"><?php echo countActiveOrInactiveOf('residents', 'active', 'active', $conn,''); ?> (100%)</span></span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Age (Active)</h3>
                            </div>
                            <div class="card-body">  
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="chart-responsive">
                                            <div class="chartjs-size-monitor">
                                            <div class="chartjs-size-monitor-expand">
                                                <div class="">

                                                </div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink">
                                                <div class="">

                                                </div>
                                            </div>
                                        </div>
                                        <canvas id="donutAge" style="min-height: 256px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 351px;" width="351" height="250" class="chartjs-render-monitor"></canvas>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                
                        <!-- /.card -->

            </section>
                    <!-- /.content -->
    </div>
                <!-- /.content-wrapper -->

                <?php include 'templates/tmp_footer.php' ?>
                <script src="../plugins2/chart.js/Chart.min.js"></script>
                <script>
                    $(function() {

                        var donutChartCanvas = $('#donutGender').get(0).getContext('2d')
                        var donutData = {
                        labels  : ['Male', 'Female'],
                        datasets: [
                            {
                            data: [<?php echo residentCountBy('gender','male', $conn) ?>,<?php echo residentCountBy('gender','female', $conn) ?>],
                            backgroundColor : ['#007bff', '#e83e8c'],
                            }
                        ]
                        }
                        var donutOptions = {
                            maintainAspectRatio : false,
                            responsive : true,
                            legend : false
                        }
                        //Create pie or douhnut chart
                        // You can switch between pie and douhnut using the method below.
                        var donutChart = new Chart(donutChartCanvas, {
                            type: 'doughnut',
                            data: donutData,
                            options: donutOptions      
                        })

                        var donutChartCanvas = $('#donutVoters').get(0).getContext('2d')
                        var donutData = {
                        labels  : ['Registered', 'Not Registered'],
                        datasets: [
                            {
                            data: [<?php echo residentCountBy('registered_voter','yes', $conn) ?>,<?php echo residentCountBy('registered_voter','no', $conn) ?>],
                            backgroundColor : ['#28a745', '#dc3545'],
                            }
                        ]
                        }
                        var donutOptions = {
                            maintainAspectRatio : false,
                            responsive : true,
                            legend : false
                        }
                        //Create pie or douhnut chart
                        // You can switch between pie and douhnut using the method below.
                        var donutChart = new Chart(donutChartCanvas, {
                            type: 'pie',
                            data: donutData,
                            options: donutOptions      
                        })

                        let teenage = <?php echo residentCountByAge($teenage, $yearNow, $conn) ?>;
                        let adult = <?php echo residentCountByAge($adult, $adult + 2, $conn) ?>;
                        let twenties = <?php echo residentCountByAge($tweties, $tweties + 9, $conn) ?>;
                        let thirties = <?php echo residentCountByAge($thirties, $thirties + 9, $conn) ?>;
                        let fourties = <?php echo residentCountByAge($fourties, $fourties + 9, $conn) ?>;
                        let fifties = <?php echo residentCountByAge($fifthies, $fifthies + 9, $conn) ?>;
                        let senior = <?php echo residentCountByAge($senior, $senior + 9, $conn) ?>;
                        let oldAge = <?php echo residentCountByAge($oldAges, $oldAges + 40, $conn) ?>;

                        var donutChartCanvas = $('#donutAge').get(0).getContext('2d')
                        var donutData = {
                        labels  : ['Teenage(14 - 17) - ' + teenage, 'Adult(18 - 19) - ' + adult,'Twenties(20 - 29) - ' + twenties,'Thirties(30 - 39) - ' + thirties, 'Fourties(40 - 49) - ' + fourties,'Fifties(50 - 59) - ' + fifties,'Senior(60 - 69) - ' + senior,'Old Ages(70+)' + oldAge],
                        datasets: [
                            {
                            data: [teenage, adult, twenties, thirties, fourties, fifties, senior, oldAge],
                            backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#6f42c1','#343a40'],
                            }
                        ]
                        }
                        var donutOptions = {
                            maintainAspectRatio : false,
                            responsive : true,
                            legend : false
                        }
                        //Create pie or douhnut chart
                        // You can switch between pie and douhnut using the method below.
                        var donutChart = new Chart(donutChartCanvas, {
                            type: 'line',
                            data: donutData,
                            options: donutOptions      
                        })
                    })

                    function isNotEmpty(caller) {
                        if (caller.val() == '') {
                            caller.addClass('is-invalid');
                            toastr.error("Please check your inputs!");
                            return false;
                        } else {
                            caller.removeClass('is-invalid');
                            return true;
                        }

                    }
                </script>

                
                <!-- /.control-sidebar -->
        </body>
        </html>


