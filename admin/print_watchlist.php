<?php
	include "session_admin.php";
	include_once("../conn.php");
	include 'templates/tmp_print_header.php';
?>
	<div class="content-wrapper">
		<img src="../Images/logo.png" class="watermark" alt="" width="100%">
		<div class="header">
			<img src="../Images/logo.png" alt="">
			<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
			<img src="../Images/marilaoLogo2.png" alt="">
		</div>
		<div class="line">
			<h4>WATCH LIST REPORTS</h4>
		</div>
		<div class="report-content">
			<table class="table table-bordered table-striped table-bordered">
				<thead>
					<tr>
						<td><strong>Full Name (Alias)</strong></td>
						<td><strong>Case</strong></td>
						<td><strong>Updated at</strong></td>
					</tr>
				</thead>
				<tbody>
					<?php
						$sql = $conn->query("SELECT * From watch_list where active = 'active'");
						if($sql->num_rows > 0){
							while($data = $sql->fetch_array()){
							?>
								<tr>
									<td><strong><?php echo $data["last_name"] . ' ' . $data["first_name"] . ' ' . $data["middle_name"] . ' (' . $data["alias"] .')'?></strong></td>
									<td><strong><?php echo $data["case_name"] ?></strong></td>
									<td><strong><?php echo $data["updated_at"] ?></strong></td>
								</tr>	
							<?php
							}
						}
					?>
				</tbody>
			</table>
			
		</div>
	</div>
	<!-- Bootstrap -->
    <script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>