<?php
	include "session_admin.php";
	include_once("../conn.php");
	include 'templates/tmp_print_header.php';

	$blotterID = $_GET["trID"];
	$sql = $conn->query("SELECT * From tanod_reports where id = '$blotterID'");
	
	if($sql->num_rows > 0){
		$data = $sql->fetch_array();
		$tanod_officer = $data["tanod_officer"];
		$details = $data["details"];
		$place = $data["place"];
		$date = $data["date"];
	}
?>
	<div class="content-wrapper">
		<img src="../Images/logo.png" class="watermark" alt="" width="100%">
		<div class="header">
			<img src="../Images/logo.png" alt="">
			<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
			<img src="../Images/marilaoLogo2.png" alt="">
		</div>
		<div class="line">
			<h4>OFFICE OF THE SANGGUNIANG BARANGAY</h4>
		</div>
		<div class="report-content">
			<br>
			<h3><?php echo $date ?></h3>
			<br>
			<br>
			<h3><?php echo $place?></h3>
			<br>
			<br>
			<table class="table">
				<tr>
					<td colspan="2"><h3>Tanod Officer: <?php echo $tanod_officer; ?></h3></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-right: 8rem"><br><br><h3><?php echo $details; ?></h3><br><br><br><br></td>
				</tr>
			</table>
		</div>
	</div>
	<!-- Bootstrap -->
    <script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>