<?php
    include "session_admin.php";
    include_once("../conn.php");
    date_default_timezone_set('Asia/Manila');
    $dateNow = date("Y.m");
    if (isset($_FILES["attachments"])) {
        $rowID = $conn-> real_escape_string($_POST['rowID']);
        $msg = "";
        $name = time() . 'resID_' . $rowID . '.jpg';
        $sql = $conn->query("SELECT id, name from resident_picture where resident_id = $rowID");
        if($sql->num_rows > 0){
            $data = $sql->fetch_array();
            $currentName = $data["name"];

            unlink("../Images/resident_picture/$currentName");

            $conn->query("UPDATE `resident_picture` SET `name`= '$name' WHERE resident_id = '$rowID'");

            move_uploaded_file($_FILES['attachments']['tmp_name'][0], "../Images/resident_picture/$name");

            $msg = array('status' => 2, "msg" => "File has been updated!", "imgName" => $name);
        }else{
            $conn->query("INSERT INTO `resident_picture`(`resident_id`, `name`, `created_at`) VALUES ('$rowID','$name','$dateNow')");

            move_uploaded_file($_FILES['attachments']['tmp_name'][0], "../Images/resident_picture/$name");

            $msg = array('status' => 1, "msg" => "File has been uploaded!", "imgName" => $name);

            
        }

        exit(json_encode($msg));
        
        // $targetFile = "../Images/resident_picture/" . basename($_FILES["attachments"]["name"][0]);
        // if(file_exists($targetFile)) {
        //     $msg = array('status' => 0, "msg" => "File already exists!");

        // }else if(move_uploaded_file($_FILES["attachments"]["tmp_name"][0], $targetFile)){
        //     
        // }

        // exit(json_encode($msg));
    }
    
    
    include 'templates/tmp_header.php';
    
?>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-file"></i>
                                <p>
                                    Documents
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="brgy_clearance.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Barangay Clearance</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="business_permit.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Business Permit</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Residents</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Data Entry</a></li>
                                <li class="breadcrumb-item active">Residents</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">

                <div id="modal" class="modal fade">
                    <div class="modal-dialog modal-lg" id="modalDialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">New Resident</h4>
                            </div>

                            <div class="modal-body">
                                
                            <div id="editContent">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active pTab" id="pills-personal-tab" data-toggle="pill" href="#pills-personal" role="tab" aria-controls="pills-personal" aria-selected="true">Personal Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pTab" id="pills-identity-tab" data-toggle="pill" href="#pills-identity" role="tab" aria-controls="pills-identity" aria-selected="false">Identification</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pTab" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pTab" id="pills-voter-tab" data-toggle="pill" href="#pills-voter" role="tab" aria-controls="pills-voter" aria-selected="false">Voter Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pTab" id="pills-households-tab" data-toggle="pill" href="#pills-households" role="tab" aria-controls="pills-households" aria-selected="false">Households</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-personal" role="tabpanel" aria-labelledby="pills-personal-tab">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" placeholder="First Name..." id="text_first_name" val="">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" placeholder="Last Name..." id="text_last_name" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Middle Name (Optional)</label>
                                                        <input type="text" class="form-control" placeholder="Middle Name..." id="text_middle_name" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Suffix (Jr, Sr)</label>
                                                        <select id="text_special" class="form-control">
                                                            <option value="">...</option>
                                                            <option value="jr">Jr</option>
                                                            <option value="sr">Sr</option>
                                                            <option value="ii">II</option>
                                                            <option value="iii">III</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Place of Birth</label>
                                                        <input type="text" class="form-control" placeholder="Ex. Bulacan..." id="text_pob" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Date of Birth (Click icon)</label>
                                                        <input type="date" class="form-control" placeholder="1994/08/15" onchange="age();" id="text_dob" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Age (Auto)</label>
                                                        <input type="text" class="form-control" placeholder="Age..." id="text_age" val="" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Gender</label>
                                                        <select id="text_gender" class="form-control">
                                                        <option value="">...</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <textarea id="text_address" class="form-control" placeholder="Address..." val=""></textarea>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Occupation</label>
                                                        <input type="text" class="form-control" placeholder="Ex. IT" id="text_occupation" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Citizenship</label>
                                                        <input type="text" class="form-control" placeholder="Ex. Filipino..." id="text_citizenship" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Civil Status</label>
                                                        <select id="text_status" class="form-control">
                                                            <option value="">...</option>
                                                            <option value="single">Single</option>
                                                            <option value="married">Married</option>
                                                            <option value="widow">Widow</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-identity" role="tabpanel" aria-labelledby="pills-identity-tab" style="width: 100%;">
                                        <div class="container">
                                            <div class="row" >
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Presented ID</label>
                                                        <select id="text_pres_id" class="form-control">
                                                            <option value="">...</option>
                                                            <option value='umid'>UMID</option>
                                                            <option value="license">LICENSE</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label>ID Number (Optional)</label>
                                                        <input type="text" class="form-control" placeholder="123343556" id="text_pres_id_no" val="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" style="width: 100%;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact Number</label>
                                                        <input type="text" class="form-control" placeholder="0919193834 / 2345678" id="text_contact" val="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email (Optional)</label>
                                                        <input type="email" class="form-control" placeholder="Ex. test@gmail.com" id="text_email" val="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-voter" role="tabpanel" aria-labelledby="pills-voter-tab" style="width: 100%;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Registered Voter ?</label>
                                                        <select id="text_voter" class="form-control">
                                                            <option value="">...</option>
                                                            <option value='yes'>Yes</option>
                                                            <option value="no">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Voters ID (Optional)</label>
                                                        <input type="text" class="form-control" placeholder="Ex. 123 232 434345" id="text_voter_id" val="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-households" role="tabpanel" aria-labelledby="pills-households-tab" style="width: 100%;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Household Name</label>
                                                        <input type="text" class="form-control" id="text_household_name" readonly>
                                                    </div>
                                                    
                                                </div>

                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label for="">Household Details</label>
                                                        <textarea id="text_household_details" class="form-control" readonly></textarea>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-12">
                                                    <div id="accordion">
                                                        <div class="card">
                                                            <div class="card-header" id="headingOne">
                                                            <h5 class="mb-0">
                                                                <button class="btn btn-link" data-toggle="collapse" data-target="#table_households" aria-expanded="true" aria-controls="table_households"
                                                                onclick="renderHouseholdMembers()">
                                                                Household Members
                                                                </button>
                                                            </h5>
                                                            </div>

                                                            <div id="table_households" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                            <div class="card-body p-0">
                                                                <table class="table table-hover text-nowrap" id="h_table">
                                                                    <thead class="text-dark">
                                                                        <tr>
                                                                            <td>ID</td>
                                                                            <td>Resident Name</td>
                                                                            <td>Relation to head</td>
                                                                            <td>Status</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="h_tbody">
                                                                    
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </div>
                                <div id="imageContainer">
                                    <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active pTab" id="pills-photo-tab" data-toggle="pill" href="#pills-photo" role="tab" aria-controls="pills-photo" aria-selected="true">Photo</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pTab" id="pills-upload-tab" data-toggle="pill" href="#pills-upload" role="tab" aria-controls="pills-upload" aria-selected="false">Upload</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pTab" id="pills-camera-tab" data-toggle="pill" href="#pills-camera" role="tab" aria-controls="pills-camera" aria-selected="false">Camera</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-photo" role="tabpanel" aria-labelledby="pills-photo-tab" style="width: 100%">
                                        <div class="d-flex justify-content-center" style="width: 100%; position: relative;">
                                                <div class="cameraResult">
                                                </div>
                                                <button class="btn btn-sm btn-danger btn-flat" id="btn-deletePicture" onclick="deletePicture();"><i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                                
                                        </div>
                                        <div class="tab-pane fade" id="pills-upload" role="tabpanel" aria-labelledby="pills-upload-tab" style="width: 100%;">
                                            <div class="row" style="width: 100%;">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div id="dropZone"> 
                                                            <center><h3>Drag & Drop Picture</h3>
                                                            <input type="file" class="" id="fileupload" name="attachments[]"></center>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <b>
                                                        <div id="progress">
                                                            
                                                        </div>
                                                    </b>
                                                </div> 
                                            </div>
                                            
                                        </div>
                                        <div class="tab-pane fade" id="pills-camera" role="tabpanel" aria-labelledby="pills-camera-tab" style="width: 100%;">
                                            
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="camera">
                                                    </div>
                                                </div>
                                                   
                                                <div class="col-md-5">
                                                    <button id="btn-camera" onclick="cameraOn()" class="btn btn-lg btn-default btn-flat btn-block">Camera On</button>
                                                    
                                                    <button class="btn btn-lg btn-info btn-flat btn-block" id="btn-shot" onclick="take_snapshot()">Capture and Save</button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="editRowID" value="0">
                                </div>
                            </div>
                        

                            <div class="modal-footer">
                                <input type="button" id="btn_manage_data" value="save" onclick="manageData('addNew')" class="btn btn-success btn-block btn-flat">
                                <input type="button" id="btn_close_modal" style="display: none;" class="btn btn-default btn-block btn-flat" data-dismiss="modal" value="Close">
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <input type="button" id="addNew" class="btn btn-primary btn-flat btn-sm" value="+ Add Resident">
                            
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-sm table-hover table-bordered" id="mainTable">
                                    <thead style="font-weight: 500;">
                                        <tr>
                                            <td>ID</td>
                                            <td>Lastname, Firstname Middlename. (Full Name)</td>
                                            <td>Active</td>
                                            <td>Created at</td>
                                            <td>Tools</td>
                                        </tr>
                                    </thead>
                                    <tbody id="mainTableBody">
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php 
        include 'templates/tmp_footer.php';
    ?>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#addNew").on('click', function() {
                $("#text_first_name").val('');
                $("#text_last_name").val('');
                $("#text_middle_name").val('');
                $("#text_contact").val('');
                $("#text_age").val('');
                $("#text_gender").val('');
                $("#text_special").val('');
                $("#text_voter_id").val('');
                $("#text_occupation").val('');
                $("#text_pob").val('');
                $("#text_citizenship").val('');
                $("#text_dob").val('');
                $("#text_pres_id_no").val('');
                $("#text_email").val('');
                $("#text_address").val('');
                $("#text_pres_id").val('');
                $("#text_voter").val('');
                $("#text_status").val('');
                $("#text_updated_at").val('');
                $("#text_active").val('');
                $('#btn_close_modal').hide();
                $('.cameraResult').html('');
                $('#imageContainer').hide();
                $('#editContent').show();
                // hide households in addnew 
                $('#pills-households-tab').hide();

                $('#modalDialog').attr('class','modal-dialog modal-lg');
                
                $('#btn_manage_data').val('Save').attr('onclick', 'manageData("addNew")').show();
                $('.modal-title').html('New Resident');
                $("#modal").modal('show');
            });

            getExistingData(0, 10);
            $('.select2').select2();

            $("#modal").on('hidden.bs.modal', function(){
                $('#table_households').removeClass('show');
                $('.tab-pane').removeClass('show active');
                $('.pTab').removeClass('active');
                $('#pills-personal-tab').addClass('active');
                $('#pills-personal').addClass('show active');
            });

            
            $('#fileupload').fileupload({
                    url: 'residents.php',
                    dropZone: '#dropZone',
                    dataType: 'json',
                    autoUpload: false
            }).on('fileuploadadd', function (e, data) {
                var fileTypeAllowed = /.\.(gif|jpg|jpeg|png)$/i;
                var fileName = data.originalFiles[0]['name'];
                var fileSize = data.originalFiles[0]['size'];

                if(!fileTypeAllowed.test(fileName)) {
                    alert('Only images is allowed!');
                }else if(fileSize > 500000) {
                    alert('Your image is too big! Max allowed is 500 KB');
                }else{
                    data.submit();
                }                   
            }).on('fileuploaddone', function (e, data) {
                var status = data.jqXHR.responseJSON.status;
                var msg = data.jqXHR.responseJSON.msg;
                var imgName = data.jqXHR.responseJSON.imgName;
                if(status == 1) {
                    toastr.success('Picture Uploaded!');
                    imageResult(imgName);
                    $('#progress').html('');
                    photoTabActive();
                }else if(status == 2) {
                    toastr.success('Picture Updated!');
                    imageResult(imgName);
                    photoTabActive();
                    $('#progress').html('');
                }else{
                    toastr.error('Upload Error!');
                }
                console.log(msg);
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress").html("Completed: " + progress + "%");
            }).bind('fileuploadsubmit', function (e, data) {
                var rowID = $('#editRowID');
                data.formData = {
                    rowID: rowID.val()
                };
            });
        });

        function age() {
            const yearNow = <?php echo $dateNow; ?>;
            const dobVal = $('#text_dob').val();
            const dob = dobVal.replace('-','.').substr(0,7);
            const ageVal = yearNow - dob;
            const age = Math.floor(ageVal);
            $('#text_age').val(age);
        }

        function viewORedit(rowID, type) {
            $.ajax({
                url: 'functions/ajax_residents.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'getRowData',
                    rowID: rowID
                },
                success: function(response) {
                    if (type == "view") {

                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_middle_name").val(response.middle_name);
                        $("#text_contact").val(response.contact);
                        $("#text_gender").val(response.gender);
                        $("#text_special").val(response.special);
                        $("#text_email").val(response.email);
                        $("#text_voter_id").val(response.voter_id);
                        $("#text_occupation").val(response.occupation);
                        $("#text_citizenship").val(response.citizenship);
                        $("#text_pob").val(response.pob);
                        $("#text_dob").val(response.date_of_birth);
                        $("#text_address").val(response.address);
                        $("#text_pres_id").val(response.pres_id);
                        $("#text_pres_id_no").val(response.pres_id_no);
                        $("#text_voter").val(response.voter);
                        $("#text_status").val(response.status);
                        $("#text_updated_at").val(response.updated_at);
                        $("#text_household_name").val(response.household_name);
                        $("#text_household_details").val(response.household_details);
                        
                        $('#btn_close_modal').show();
                        $('#btn_manage_data').hide();
                    } else {
                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_middle_name").val(response.middle_name);
                        $("#text_contact").val(response.contact);
                        $("#text_gender").val(response.gender);
                        $("#text_special").val(response.special);
                        $("#text_email").val(response.email);
                        $("#text_voter_id").val(response.voter_id);
                        $("#text_occupation").val(response.occupation);
                        $("#text_citizenship").val(response.citizenship);
                        $("#text_pob").val(response.pob);
                        $("#text_dob").val(response.date_of_birth);
                        $("#text_address").val(response.address);
                        $("#text_pres_id").val(response.pres_id);
                        $("#text_pres_id_no").val(response.pres_id_no);
                        $("#text_voter").val(response.voter);
                        $("#text_status").val(response.status);
                        $("#text_updated_at").val(response.updated_at);
                        $("#text_household_name").val(response.household_name);
                        $("#text_household_details").val(response.household_details);
                        $('#btn_close_modal').hide();
                        $('#btn_manage_data').val('Save changes').attr('onclick', 'manageData("updateRow")');
                        $('#btn_manage_data').show();
                        
                    }
                    age();

                    // show households in viewORedit 
                    $('#pills-households-tab').show();

                    $('#modalDialog').attr('class','modal-dialog modal-lg');

                    $('#imageContainer').hide();
                    $('#editContent').show();
                    $('.clearance').show();
                    $('.modal-title').html(response.last_name + ', ' +response.first_name);
                    $('#editRowID').val(rowID);
                    $("#modal").modal('show');
                }
            });
        }

        

        function updateRowStatus(id, status){
            var statusLbl = "";
            var statusBtn = "";
            $.ajax({
                url: 'functions/ajax_residents.php',
                method: 'POST',
                dataType: 'text',   
                data: {
                    key: 'updateRowStatus',
                    status: status,
                    rowID: id
                }, success: function (response){
                    if(response == "Active updated"){
                        if(status == 'active'){
                            status = 'inactive';
                            statusLbl = "Inactive";
                            statusBtn = "danger";
                        }else{
                            status = 'active';
                            statusLbl = "Active";
                            statusBtn = "success";
                        }
                        $("#status_"+id).html("<button onclick='updateRowStatus("+id+",\""+status+"\")' class='btn btn-"+statusBtn+" btn-flat btn-sm' type='button'>"+statusLbl+"</button>"); 
                    }else{
                        alert(response);
                    }
                }
            });
        }

        function addedRow(){
            $.ajax({
                url: 'functions/ajax_residents.php',
                method: 'POST',
                dataType: 'text',   
                data: {
                    key: 'addRow',
                }, success: function (response){
                    $("tbody tr:first").before(response);
                }
            });
        }

        function getExistingData(start, limit) {
            $.ajax({
                url: 'functions/ajax_residents.php',
                method: 'POST',
                dataType: 'text',
                data: {
                    key: 'getExistingData',
                    start: start,
                    limit: limit
                },
                success: function(response) {
                    if (response != "reachedMax") {
                        $('tbody#mainTableBody').append(response);
                        start += limit;
                        getExistingData(start, limit);
                    } else {
                        $("#mainTable").DataTable({
                                "order": [[ 0, "desc" ]]
                            });
                    }
                }
            });
        }

        function manageData(key) {
            var first_name = $("#text_first_name");
            var last_name = $("#text_last_name");
            var middle_name = $("#text_middle_name");
            var occupation = $("#text_occupation");
            var pob = $("#text_pob");
            var pres_id_no = $("#text_pres_id_no");
            var citizenship = $("#text_citizenship");
            var voter_id = $("#text_voter_id");
            var address = $("#text_address");
            var contact = $("#text_contact");
            var pres_id = $("#text_pres_id");
            var status = $("#text_status");
            var email = $("#text_email");
            var voter = $("#text_voter");
            var dob = $("#text_dob");
            var gender = $("#text_gender");
            var special = $("#text_special");
            var editRowID = $("#editRowID");

            if (isNotEmpty(first_name) && isNotEmpty(last_name) && isNotEmpty(pob) && isNotEmpty(dob) && isNotEmpty(gender) && isNotEmpty(address) && isNotEmpty(occupation) && isNotEmpty(citizenship) && isNotEmpty(status) &&isNotEmpty(contact) &&isNotEmpty(pres_id)) {
                $.ajax({
                    url: 'functions/ajax_residents.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: key,
                        first_name: first_name.val(),
                        last_name: last_name.val(),
                        middle_name: middle_name.val(),
                        address: address.val(),
                        dob: dob.val(),
                        gender: gender.val(),
                        special: special.val(),
                        email: email.val(),
                        occupation: occupation.val(),
                        pob: pob.val(),
                        pres_id_no: pres_id_no.val(),
                        citizenship: citizenship.val(),
                        voter_id: voter_id.val(),
                        status: status.val(),
                        pres_id: pres_id.val(),
                        voter: voter.val(),
                        contact: contact.val(),
                        rowID: editRowID.val()
                    },
                    success: function(response) {
                        if (response == "Data added") {
                            toastr.success(response);
                            addedRow();
                        } else if (response == "Data updated") {
                            toastr.info(response);
                            $("#name_" + editRowID.val()).html(last_name.val() + ', '+first_name.val() + ' '+middle_name.val() + '.');
                        } else {
                            toastr.error(response);
                        }
                        $("#modal").modal('hide');
                        $("#btn_manage_data").attr('value', 'Save').attr('onclick', "manageData('addNew')");
                    }
                });
            }
        }

        function deletePicture(){
            var rowID = $("#editRowID");
            if (isNotEmpty(rowID)){
                $.ajax({
                    url: 'functions/picture.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        deletePicture: '1',
                        rowID: rowID.val() 
                    }, success: function (response){
                        if(response == 'Picture Deleted'){
                            imageResult('no_image.jpg');
                            toastr.info(response);
                        }
                        if(response == 'No picture to delete'){
                            toastr.error(response);
                        }
                    }
                });
            }    
        }

        function viewOReditImg(id){
            $('#editRowID').val(id);
            $('#editContent').hide();
            $.ajax({
                    url: 'functions/picture.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        viewPicture: '1',
                        rowID: id 
                    }, success: function (response){
                        if(response != "No Picture Found!"){
                            $('#btn-deletePicture').show();
                            imageResult(response);
                        }else{
                            imageResult('no_image.jpg');
                            $('#btn-deletePicture').hide();
                        }
                        photoTabActive();
                        $('#imageContainer').show();
                        $('#btn_manage_data').hide();
                        $('#btn_close_modal').show();
                        $('#modalDialog').attr('class','modal-dialog modal-md');
                        $("#modal").modal('show');
                    }
                });
            
            // alert('asdasd')
        }

        function photoTabActive(){
            $('.tab-pane').removeClass('show active');
            $('.pTab').removeClass('active');
            $('#pills-photo-tab').addClass('active');
            $('#pills-photo').addClass('show active');
        }

        function cameraOn(){
            Webcam.set({
                width: 320,
                height: 240,
                crop_width: 240,
                crop_height: 240
	        });
            Webcam.attach( '.camera' );
            $('#btn-camera').attr('onclick', 'cameraOff()').html('Camera Off');
            $('#btn-camera').removeClass('btn-default');
            $('#btn-camera').addClass('btn-danger');
        }

        function imageResult(src){
            $('.cameraResult').html('<img src="../Images/resident_picture/'+src+'" heigth="240px" width="240px"/>');
        }

        function cameraOff(){
            Webcam.reset();
            $('#btn-camera').attr('onclick', 'cameraOn()').html('Camera On');
            $('#btn-camera').removeClass('btn-danger');
            $('#btn-camera').addClass('btn-default');
        }

		function take_snapshot() {
            var rowID = $("#editRowID");
            if (isNotEmpty(rowID)){
                Webcam.snap( function(data_uri) {
                    var imageName = 'resID_' + rowID.val();
                    var url = 'functions/picture.php?name=' + imageName + '&rowID=' + rowID.val();
                    $('.cameraResult').html('<img src="'+data_uri+'" />');
                    Webcam.upload( data_uri, url, function(code, text) {
                        // Upload complete!
                        // 'code' will be the HTTP response code from the server, e.g. 200
                        // 'text' will be the raw response content
                        toastr.success(text);
                    });
                    photoTabActive();
                });
                $('#btn-camera').attr('onclick', 'cameraOff()').html('Camera On');
                $('#btn-camera').removeClass('btn-danger');
                $('#btn-camera').addClass('btn-default');
                Webcam.reset();
            }
			
		}

        function renderHouseholdMembers(){
            var rowID = $("#editRowID");
            $.ajax({
                url: 'functions/ajax_residents.php',
                method: 'POST',
                dataType: 'text',   
                data: {
                    key: 'renderHouseholdMembers',
                    rowID: rowID.val() 
                }, success: function (response){
                    $('tbody#h_tbody').html(response);
                    $('#h_table_container').show();
                }
            });
        }

        

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.addClass('is-invalid');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.removeClass('is-invalid');
                return true;
            }
        }
    </script>
</body>
</html>