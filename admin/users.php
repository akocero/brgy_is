<?php
    include "session_admin.php";
    include_once("../conn.php");
    include 'templates/tmp_header.php';
?>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Users</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Tools</a></li>
                                <li class="breadcrumb-item active">Users</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">

                <div id="modal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add/Update category</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row" id="editContent">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" placeholder="First Name..." id="text_first_name" val="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name..." id="text_last_name" val="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" placeholder="Username..." id="text_username" val="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" id="text_password" class="form-control" placeholder="Password" val="" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Security Question</label>
                                            <select id="text_s_question" class="form-control">
                                                <option value=""></option>
                                                <option value="favColor">Favorite Color?</option>
                                                <option value="favNumber">Favorite Number?</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Answer</label>
                                            <input type="text" class="form-control" placeholder="Answer..." id="text_s_answer" val="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select id="text_type" class="form-control">
                                                <option value=""></option>
                                                <option value="admin">Admin</option>
                                                <option value="user">User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="editRowID" value="0">
                                </div>
                            </div>
                        

                            <div class="modal-footer">
                                <input type="button" id="btn_manage_data" value="save" onclick="manageData('addNew')" class="btn btn-success btn-block btn-flat">
                                <input type="button" id="btn_close_modal" style="display: none;" class="btn btn-default btn-block btn-flat" data-dismiss="modal" value="Close">
                            </div>
                        </div>
                    </div>
                </div>


        <div class="card">
            <div class="card-header">
                <input type="button" id="addNew" class="btn btn-primary btn-flat btn-sm" value="+ Add User">
                
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <table class="table table-sm table-hover table-bordered">
                        <thead style="font-weight: 500;">
                            <tr>
                                <td>ID</td>
                                <td>Full Name</td>
                                <td>Active</td>
                                <td>Created On</td>
                                <td>Tools</td>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>


            </div>
            <!-- /.card-body -->
        </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'templates/tmp_footer.php'; ?>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#addNew").on('click', function() {
                $("#text_first_name").val('');
                $("#text_last_name").val('');
                $("#text_username").val('');
                $("#text_s_answer").val('');
                $("#text_s_question").val('');
                $("#text_password").val('');
                $("#text_type").val('');
                $('#btn_close_modal').hide();
                $('#btn_manage_data').val('Save').attr('onclick', 'manageData("addNew")').show();
                $("#modal").modal('show');
            });
            getExistingData(0, 10);

        });

        function viewORedit(rowID, type) {
            $.ajax({
                url: 'functions/ajax_users.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'getRowData',
                    rowID: rowID
                },
                success: function(response) {
                    if (type == "view") {

                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_username").val(response.username);
                        $("#text_s_answer").val(response.s_answer);
                        $("#text_s_question").val(response.s_question);
                        $("#text_password").val(response.password);
                        $("#text_type").val(response.type);

                        $('#btn_close_modal').show();
                        $('#btn_manage_data').hide();
                    } else {
                        $("#text_first_name").val(response.first_name);
                        $("#text_last_name").val(response.last_name);
                        $("#text_username").val(response.username);
                        $("#text_s_answer").val(response.s_answer);
                        $("#text_s_question").val(response.s_question);
                        $("#text_password").val(response.password);
                        $("#text_type").val(response.type);
                        $('#btn_close_modal').hide();
                        $('#btn_manage_data').val('Save changes').attr('onclick', 'manageData("updateRow")');
                        $('#btn_manage_data').show();

                    }
                    $('#editRowID').val(rowID);
                    $("#modal").modal('show');
                }
            });

        }

        function updateRowStatus(id, status){
                var statusLbl = "";
                var statusBtn = "";
                $.ajax({
                    url: 'functions/ajax_users.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'updateRowStatus',
                        status: status,
                        rowID: id
                    }, success: function (response){
                        if(response == "Active updated"){
                            if(status == 'active'){
                                status = 'inactive';
                                statusLbl = "Inactive";
                                statusBtn = "danger";
                            }else{
                                status = 'active';
                                statusLbl = "Active";
                                statusBtn = "success";
                            }
                           $("#status_"+id).html("<button onclick='updateRowStatus("+id+",\""+status+"\")' class='btn btn-"+statusBtn+" btn-flat btn-sm' type='button'>"+statusLbl+"</button>"); 
                        }else{
                            alert(response);
                        }
                    }
                });
            }

        function addedRow(){
                $.ajax({
                    url: 'functions/ajax_users.php',
                    method: 'POST',
                    dataType: 'text',   
                    data: {
                        key: 'addRow',
                    }, success: function (response){
                        $("tbody tr:first").before(response);
                    }
                });
            }

        function getExistingData(start, limit) {
            $.ajax({
                url: 'functions/ajax_users.php',
                method: 'POST',
                dataType: 'text',
                data: {
                    key: 'getExistingData',
                    start: start,
                    limit: limit
                },
                success: function(response) {
                    if (response != "reachedMax") {
                        $('tbody').append(response);
                        start += limit;
                        getExistingData(start, limit);
                    } else {
                        $(".table").DataTable({
                                "order": [[ 0, "desc" ]]
                            });
                    }
                }
            });
        }

        function manageData(key) {
            var first_name = $("#text_first_name");
            var last_name = $("#text_last_name");
            var s_answer = $("#text_s_answer");
            var username = $("#text_username");
            var password = $("#text_password");
            var type = $("#text_type");
            var s_question = $("#text_s_question");
            var editRowID = $("#editRowID");

            if (isNotEmpty(first_name) && isNotEmpty(last_name) && isNotEmpty(username) && isNotEmpty(password) && isNotEmpty(type) && isNotEmpty(s_question) && isNotEmpty(s_answer)) {
                $.ajax({
                    url: 'functions/ajax_users.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: key,
                        first_name: first_name.val(),
                        last_name: last_name.val(),
                        username: username.val(),
                        password: password.val(),
                        type: type.val(),
                        s_question: s_question.val(),
                        s_answer: s_answer.val(),
                        rowID: editRowID.val()
                    },
                    success: function(response) {
                        if (response == "Data added") {
                            toastr.success(response);
                            addedRow();
                        } else if (response == "Data updated") {
                            toastr.info(response);
                            $("#name_" + editRowID.val()).html(last_name.val() + ', '+first_name.val());
                        } else {
                            toastr.error(response);
                        }
                        $("#modal").modal('hide');
                        $("#btn_manage_data").attr('value', 'Save').attr('onclick', "manageData('addNew')");
                    }
                });
            }
        }

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.parent('div').addClass('has-error');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.parent('div').removeClass('has-error');
                return true;
            }

        }
    </script>
</body>

</html>