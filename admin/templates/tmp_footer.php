<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019-2020 <a href="http://adminlte.io">Brgy Sta. Rosa 1</a>.</strong> All rights
    reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-light text-center">
    <!-- Control sidebar content goes here -->
    <div class="p-3 control-sidebar-content">
        <p class="mb-0 " id="accountName"></p>
        <p id="accountType" class=""></p>
        <button id="btnManageAccount" class="btn btn-link btn-sm pl-0">Manage Account</button>
        <button id="btnManagePassword" class="btn btn-link btn-sm pl-0 mt-1">Change Password</button>   
        <hr>
        <a href="../session_destroy.php" class="btn btn-default btn-block">Sign out</a>
    </div> 
</aside>

<?php
    $user_id = $_SESSION["user_id"];
?>

<div id="modalAccount" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Account Details</h5>
            </div>
            <div class="modal-body">
                <div class="row" id="ManageAccount">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Account Type : <span id="text_accnt_type"></span></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">First Name</label>
                            <input id="text_accnt_first_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Last Name</label>
                            <input id="text_accnt_last_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input id="text_accnt_username" type="text" class="form-control" val="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Enter Password</label>
                            <input id="text_accnt_password" type="password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row" id="ManagePassword">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Enter Old Password</label>
                            <input id="text_old_password" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Enter New Password</label>
                            <input id="text_accnt_new_password" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Enter New Password Again</label>
                            <input id="text_accnt_duplicate_password" type="password" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn_saveAccntChanges">Save Changes</button>
            </div>
        </div>
    </div>
</div>

</div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="../plugins2/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
    <!-- select2 -->
    <script src="../plugins2/select2/js/select2.full.min.js"></script>
    <!-- toaster -->
    <script src="../plugins2/toastr/toastr.min.js"></script>
    <!-- datatables -->
    <script src="../plugins2/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins2/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="../plugins2/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../plugins2/adminLTE/dist/js/adminlte.js"></script>
    <script src="../plugins2/inputNumbersOnly.js"></script>
    <!-- webcam.js  -->
    <script src="../plugins2/webcam.js/webcam.min.js"></script>
    <!-- file upload -->
    <script src="../plugins2/jQuery-File-Upload/jquery.ui.widget.js"></script>
    <script src="../plugins2/jQuery-File-Upload/jquery.iframe-transport.js"></script>
    <script src="../plugins2/jQuery-File-Upload/jquery.fileupload.js"></script>
    <script>
        $(function(){
            $('#btnManageAccount').on('click', function(){
                $('#ManageAccount').show();
                $('#ManagePassword').hide();
                $("#modalAccount").modal('show');
                $('#btn_saveAccntChanges').attr('onclick','manageAccntDetails(' + accountID + ')');
            });
            $('#btnManagePassword').on('click', function(){
                $('#ManageAccount').hide();
                $('#ManagePassword').show();
                $("#modalAccount").modal('show');
                $('#btn_saveAccntChanges').attr('onclick','manageAccntSecurity(' + accountID + ')');
            });
            $('#btnRightBarToggler').on('click', function(){
                renderDetails();
            });

            $('#text_accnt_duplicate_password').on('keyup', function(){
                validateNewPassword();
            });

            $('#text_accnt_new_password').on('keyup', function(){
                validateNewPassword();
            });

            $('#btn-reload').on('click', function(){
                window.location.reload();
            });
        }); 

        const accountID = <?php echo $user_id; ?>;

        function validateNewPassword(){
            var duplicate_pass = $('#text_accnt_duplicate_password');
            var new_pass = $('#text_accnt_new_password');

            if(duplicate_pass.val() != new_pass.val()){
                duplicate_pass.addClass('is-invalid');
                new_pass.addClass('is-invalid');
            }else{
                duplicate_pass.removeClass('is-invalid');
                duplicate_pass.addClass('is-valid');
                new_pass.removeClass('is-invalid');
                new_pass.addClass('is-valid');
            }
        }

        function renderDetails(){
            $.ajax({
                url: 'functions/ajax_manage_accnt.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'renderDetails',
                    accountID: accountID
                },
                success: function(response){
                    $('#accountName').html(response.last_name + ', ' + response.first_name);
                    $('#accountType').html(response.type);
                    $('#text_accnt_type').html(response.type);
                    $('#text_accnt_last_name').val(response.last_name);
                    $('#text_accnt_first_name').val(response.first_name);
                    $('#text_accnt_username').val(response.username);
                }
            });
        }

        function manageAccntDetails(accountID){
            var accnt_first_name = $('#text_accnt_first_name');
            var accnt_last_name = $('#text_accnt_last_name');
            var accnt_username = $('#text_accnt_username');
            var accnt_password = $('#text_accnt_password');
            if(isNotEmpty(accnt_first_name) && isNotEmpty(accnt_last_name) && isNotEmpty(accnt_username) && isNotEmpty(accnt_password)){
                $.ajax({
                    url: 'functions/ajax_manage_accnt.php',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        key: 'manageDetails',
                        accountID: accountID,
                        accnt_last_name: accnt_last_name.val(),
                        accnt_first_name: accnt_first_name.val(),
                        accnt_password: accnt_password.val(),
                        accnt_username: accnt_username.val()
                    },
                    success: function(response){
                        if(response == 'Account Updated'){
                            toastr.success(response);
                            $("#modalAccount").modal('hide');
                        }else{
                            toastr.error(response);
                        }
                        accnt_password.val('');
                    }
                });
            }
        }

        function manageAccntSecurity(accountID){
            var accnt_new_password = $("#text_accnt_new_password");
            var accnt_duplicate_password = $("#text_accnt_duplicate_password");
            var accnt_old_password = $("#text_old_password");
            if(isNotEmpty(accnt_old_password) && isNotEmpty(accnt_new_password) && isNotEmpty(accnt_duplicate_password) && accnt_new_password.val() == accnt_duplicate_password.val()){
                $.ajax({
                    url: 'functions/ajax_manage_accnt.php',
                    method: 'POST', 
                    dataType: 'text',
                    data: {
                        key: 'manageSecurity',
                        accountID: accountID,
                        accnt_new_password: accnt_new_password.val(),
                        accnt_duplicate_password: accnt_duplicate_password.val(),
                        accnt_old_password: accnt_old_password.val()
                    },
                    success: function(response){
                        if(response == 'Account Updated'){
                            toastr.success(response);
                            $("#modalAccount").modal('hide');
                            accnt_new_password.val('');
                            accnt_duplicate_password.val('');
                            accnt_old_password.val('');
                        }else{
                            toastr.error(response);
                        }

                    }
                });
            } 
        }
    </script>