<!DOCTYPE html>
<html lang="en">
<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>STI Tracker | Summary</title>    
    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="../plugins2/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../plugins2/fontawesome/css/all.min.css">
    <!-- toaster -->
    <link rel="stylesheet" href="../plugins2/toastr/toastr.min.css">
    <!-- select2 -->
    <link rel="stylesheet" href="../plugins2/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="../plugins2/select2/css/select2.min.css">
    <!-- datatables -->
    <link rel="stylesheet" href="../plugins2/datatables/dataTables.bootstrap4.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../plugins2/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../plugins2/adminLTE/dist/css/adminlte.min.css">
    
    <!-- main css  -->
    <link rel="stylesheet" href="../css/main.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-warning navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link text-dark" id="btn-reload">Refresh</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn btn-outline-dark text-gray-dark" id="btnRightBarToggler" style="border-radius: 20px;" data-widget="control-sidebar" data-slide="true" href="#">
                        <i class="fas fa-user"></i> <?php echo ucfirst($_SESSION["user_first_name"]); ?>
                    </a>
                </li>
            </ul>

        </nav>
        <!-- /.navbar -->