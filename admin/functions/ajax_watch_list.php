<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from watch_list where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'middle_name' => $data['middle_name'],
			'alias' => $data['alias'],
			'case_name' => $data['case_name'],
			'details' => $data['details']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id, CONCAT(last_name, ', ', first_name,' ',middle_name,' (',alias,')') AS name, case_name as case_name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From watch_list ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["active"] == 'active'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="case_'.$data["id"].'">'.$data["case_name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id, CONCAT(last_name, ', ', first_name,' ',middle_name,' (',alias,')') AS name, case_name as case_name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From watch_list ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["active"] == 'active'){
					$status = "success";
					$statusLbl = "Active";
				}else{
					$status = "danger";
					$statusLbl = "Inactive";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="case_'.$data["id"].'">'.$data["case_name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'active'){
			$status = 'inactive';
		}else{
			$status = 'active';
		}
		$conn-> query("UPDATE `watch_list` SET `active`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Active updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$first_name = $conn-> real_escape_string($_POST['first_name']);
	$last_name = $conn-> real_escape_string($_POST['last_name']);
	$alias = $conn-> real_escape_string($_POST['alias']);
	$middle_name = $conn-> real_escape_string($_POST['middle_name']);
	$case_name = $conn-> real_escape_string($_POST['case_name']);
	$details = $conn-> real_escape_string($_POST['details']);
	
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `watch_list` SET `first_name`= '$first_name',`last_name`='$last_name',`middle_name`='$middle_name',`alias`='$alias',`case_name`='$case_name',`details`='$details',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from watch_list where first_name = '$first_name' and case_name = '$case_name' and alias = '$alias'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `watch_list`(`first_name`, `last_name`, `middle_name`, `alias`, `case_name`, `details`, `active`, `created_at`, `updated_at`) VALUES ('$first_name','$last_name','$middle_name','$alias','$case_name','$details','active','$dateNow','$dateNow')");
			exit('Data added');
		}
	}
}

?>