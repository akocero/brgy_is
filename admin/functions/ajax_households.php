<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
	date_default_timezone_set('Asia/Manila');
	$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from households where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'name' => $data['name'],
			'details' => $data['details']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id as id, name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From households ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["active"] == 'active'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="householdsMembers('.$data["id"].',\''.$data["name"].'\')">
								<i class="fa fa-users"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id as id, name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From households ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["active"] == 'active'){
					$status = "success";
					$statusLbl = "Active";
				}else{
					$status = "danger";
					$statusLbl = "Inactive";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="householdsMembers('.$data["id"].',\''.$data["name"].'\')">
								<i class="fa fa-users"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	if ($_POST['key'] == 'getHouseholdsMembers') {
		$addedQry = $conn->real_escape_string($_POST['addedQry']);
		$h_id = $conn->real_escape_string($_POST['h_id']);
		$response ="";
		$sql = $conn->query("SELECT hm.id as id, hm.relation as relation, Concat(r.last_name,', ',r.first_name) as name, hm.household_status as status, Date_format(hm.created_at, '%a, %M %d %Y - %h:%i %p') as created_at FROM household_members hm, households h, residents r Where r.id = hm.resident_id and hm.household_id = h.id and h.id = '$h_id' Order By hm.id DESC " . $addedQry);
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				$response .= '
					<tr id="mem_'.$data["id"].'">
						<td>'.ucfirst($data["name"]).'</td>
						<td>'.ucfirst($data["relation"]).'</td>
						<td>'.ucfirst($data["status"]).'</td>
						<td>'.$data["created_at"].'</td>
						<td>
							<button class="btn btn-danger btn-flat btn-sm" type = "button" onclick="deleteMember('.$data["id"].')">
								<i class="fas fa-trash"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('0');
		}
	}
	if ($_POST['key'] == 'deleteRowMember') {
		$hm_id = $conn-> real_escape_string($_POST['hm_id']);
		$conn->query("DELETE FROM `household_members` WHERE id = '$hm_id'");
		exit('Data Deleted');
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'addHouseholdMember') {
		$h_member_id = $conn-> real_escape_string($_POST['h_member_id']);
		$h_status = $conn-> real_escape_string($_POST['h_status']);
		$h_relation = $conn-> real_escape_string($_POST['h_relation']);

		if($h_status == 'head'){
			$sql = $conn->query("SELECT id from household_members where household_status = 'head' and household_id = '$rowID'");
			if ($sql->num_rows > 0) {
				exit("1 head per household");
			}else{
				$sql = $conn->query("SELECT id from household_members where resident_id = '$h_member_id'");
				if ($sql->num_rows > 0) {
					exit('Data already added');
				}else{
					$conn-> query("INSERT INTO `household_members`(`household_id`, `relation`,`household_status`, `resident_id`, `created_at`, `updated_at`) VALUES ('$rowID','$h_relation','$h_status','$h_member_id','$dateNow','$dateNow')");
					exit('Data added');
				}
			}
		}else{
			$sql = $conn->query("SELECT id from household_members where resident_id = '$h_member_id'");
			if ($sql->num_rows > 0) {
				exit('Data already added');
			}else{
				$conn-> query("INSERT INTO `household_members`(`household_id`, `relation`, `household_status`, `resident_id`, `created_at`, `updated_at`) VALUES ('$rowID','$h_relation','$h_status','$h_member_id','$dateNow','$dateNow')");
				exit('Data added');
			}
		}	
	}

	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'active'){
			$status = 'inactive';
		}else{
			$status = 'active';
		}
		$conn-> query("UPDATE `households` SET `active`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Active updated');
	}

	$name = $conn-> real_escape_string($_POST['name']);
	$details = $conn-> real_escape_string($_POST['details']);

	// Delete data
	if($_POST['key'] == 'deleteRow') {
		$conn -> query ("Delete from holidays where id='$rowID'");
		exit("holidays Deleted!");
	} 
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `households` SET `name`='$name',`details`='$details',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from households where name = '$name'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `households`(`name`, `details`,`active`,`created_at`, `updated_at`) VALUES ('$name','$details','active','$dateNow','$dateNow')");
			exit('Data added');
		}
	}
}

?>