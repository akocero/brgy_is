<?php

include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");
if(isset($_GET['name'])){
    $name = time() . $_GET['name'] . '.jpg';
    $rowID = $_GET['rowID'];
    $sql = $conn->query("SELECT id, name from resident_picture where resident_id = $rowID");
    if($sql->num_rows > 0){
        $data = $sql->fetch_array();
        $currentName = $data["name"];

        unlink("../../Images/resident_picture/$currentName");

        $conn->query("UPDATE `resident_picture` SET `name`= '$name' WHERE resident_id = '$rowID'");

        move_uploaded_file($_FILES['webcam']['tmp_name'], "../../Images/resident_picture/$name");

        exit('Picture Updated');
    }else{
        $conn->query("INSERT INTO `resident_picture`(`resident_id`, `name`, `created_at`) VALUES ('$rowID','$name','$dateNow')");

        move_uploaded_file($_FILES['webcam']['tmp_name'], "../../Images/resident_picture/$name");

        exit('Picture Uploaded');
    }
}

if(isset($_POST['deletePicture'])){
    $rowID = $conn-> real_escape_string($_POST['rowID']);

    $sql = $conn->query("SELECT id,name from resident_picture where resident_id = '$rowID'");
    if($sql->num_rows > 0){
        $data = $sql->fetch_array();
        $name = $data["name"];
        $conn->query("DELETE FROM `resident_picture` WHERE resident_id = '$rowID'");
        unlink("../../Images/resident_picture/$name");
        exit('Picture Deleted');
    }else{
        exit('No picture to delete');
    }
}

if(isset($_POST['viewPicture'])){
    $rowID = $conn-> real_escape_string($_POST['rowID']);

    $sql = $conn->query("SELECT id,name from resident_picture where resident_id = '$rowID'");
    if($sql->num_rows > 0){
        $data = $sql->fetch_array();
        $name = $data["name"];
        exit($name);
    }else{
        exit('No Picture Found!');
    }
}
?>