<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from users where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'username' => $data['username'],
			'password' => $data['password'],
			'type' => $data['type'],
			's_question' => $data['security_question'],
			's_answer' => $data['security_answer']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id as id, CONCAT(last_name, ', ', first_name) AS name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From users ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["active"] == 'active'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id as id, CONCAT(last_name, ', ', first_name) AS name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From users ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["active"] == 'active'){
					$status = "success";
					$statusLbl = "Active";
				}else{
					$status = "danger";
					$statusLbl = "Inactive";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'active'){
			$status = 'inactive';
		}else{
			$status = 'active';
		}
		$conn-> query("UPDATE `users` SET `active`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Active updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$first_name = $conn-> real_escape_string($_POST['first_name']);
	$username = $conn-> real_escape_string($_POST['username']);
	$last_name = $conn-> real_escape_string($_POST['last_name']);
	$password = $conn-> real_escape_string($_POST['password']);
	$type = $conn-> real_escape_string($_POST['type']);
	$s_answer = $conn-> real_escape_string($_POST['s_answer']);
	$s_question = $conn-> real_escape_string($_POST['s_question']);
	
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `users` SET `first_name`='$first_name',`username`='$username',`last_name`='$last_name',`type`='$type',`password`='$password',`security_answer`='$s_answer',`security_question`='$s_question', `updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from users where first_name = '$first_name' and last_name = '$last_name'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `users`(`first_name`, `username`, `last_name`, `type`, `password`, `security_answer`, `security_question`, `created_at`, `updated_at`,`active`) VALUES ('$first_name','$username','$last_name','$type','$password','$s_answer','$s_question','$dateNow','$dateNow','active')");
			exit('Data added');
		}
	}
}

?>