<?php
if (isset($_POST['key'])) {
	// db connection
include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");

function statusCheck($status) {
    $statusLbl = "";
    if($status == 'solved'){
        $status = "success";
        $statusLbl = "Solved";
    }else{
        $status = "warning";
        $statusLbl = "Pending";
    }

    $statusArray = array(
        "status" => $status,
        "statusLbl" => $statusLbl,
    );

    return $statusArray;
}

function issuedDateCheck($issuedDate, $expPeriod){
    $tableColor = "";
    $exp_date = new dateTime($expPeriod);
    $dateIssued = new dateTime($issuedDate);
	

    $interval = date_diff($dateIssued, $exp_date);
   
	$intervalMonth = $interval->format('%m');
	$intervalDay = $interval->format('%a');
    if($dateIssued > $exp_date){
		$formattedInterval =  $interval->format('%a Day/s delay (Expired)');
        $tableColor = 'table-danger';
	}else if($intervalMonth == '0'){
		$formattedInterval =  $interval->format('%a Day/s');
		$tableColor = 'table-warning';
	}else{
        $formattedInterval =  $interval->format('%m Month %d Day');
    }

    $intervalArray = array(
        "formattedInterval" => $formattedInterval,
        "tableColor" => $tableColor,
    );

    return $intervalArray;
}

	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from businesses where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'name' => $data['name'],
			'operator' => $data['operator'],
			'issued_date' => $data['issued_date'],
			'expiration_date' => $data['expiration_period'],
            'location' => $data['location'],
			'address' => $data['address'],
            'contact_no' => $data['contact_no'],
            'owner_info' => $data['owner_info']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = "";
		$sql = $conn-> query("SELECT id, name, Date_format(issued_date, '%a, %M %d %Y') as date_added, status From businesses ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			$statusArray = statusCheck($data["status"]);
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}
	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = "";
		$sql = $conn->query("SELECT id, name, Date_format(issued_date, '%a, %M %d %Y') as format_issued_date, issued_date, expiration_period, status From businesses ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
                $statusArray = statusCheck($data["status"]);
                $interval = issuedDateCheck($data['issued_date'], $data["expiration_period"]);
				$response .= '
					<tr class="'.$interval['tableColor'].'">
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$statusArray['status'].' btn-flat btn-sm" type="button">
							'.$statusArray['statusLbl'].'
							</button>
						</td>
						<td>'.$data["format_issued_date"].'</td>
						<td>'.$interval['formattedInterval'].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'solved'){
			$status = 'pending';
		}else{
			$status = 'solved';
		}
		$conn-> query("UPDATE `businesses` SET `status`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Status updated');
	}

    $name = $conn-> real_escape_string($_POST['name']);
    $location = $conn-> real_escape_string($_POST['location']);
    $address = $conn-> real_escape_string($_POST['address']);
    $contact_no = $conn-> real_escape_string($_POST['contact_no']);
	$issued_date = $conn-> real_escape_string($_POST['issued_date']);
	$expiration_date = $conn-> real_escape_string($_POST['expiration_date']);
    $operator = $conn-> real_escape_string($_POST['operator']);
    $owner_info = $conn-> real_escape_string($_POST['owner_info']);
	
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `businesses` SET `name`='$name',`location`='$location',`address`='$address',`contact_no`='$contact_no',`issued_date`='$issued_date',`expiration_period`='$expiration_date',`operator`='$operator',`owner_info`='$owner_info',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from businesses where name = '$name' and location = '$location'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `businesses`(`name`, `location`, `address`, `contact_no`, `issued_date`, `expiration_period`, `operator`, `owner_info`, `created_at`, `updated_at`) VALUES ('$name','$location','$address','$contact_no','$issued_date','$expiration_date','$operator','$owner_info','$dateNow','$dateNow')");
			exit('Data added');
		}
	}
}
?>