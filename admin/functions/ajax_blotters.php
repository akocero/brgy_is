<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
	date_default_timezone_set('Asia/Manila');
	$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from blotters where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'complainant' => $data['complainant'],
			'respondent' => $data['respondent'],
			'complain' => $data['complain'],
			'doc' => $data['details_of_complain'],
			'doi' => $data['date_of_incident'],
			'poi' => $data['place_of_incident'],
			'oi' => $data['officer_incharge'],
			'status' => $data['status']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id, complainant, complain, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, status From blotters ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["status"] == 'solved'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["complainant"].'</td>
						<td id="complain_'.$data["id"].'">'.$data["complain"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
							<a href="print_blotters.php?brID='.$data["id"].'" class="btn btn-default btn-flat btn-sm" type = "button" target="_blank">
								<i class="fa fa-print"></i>
							</a>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id, complainant, complain, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, status From blotters ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["status"] == 'solved'){
					$status = "success";
					$statusLbl = "Solved";
				}else{
					$status = "warning";
					$statusLbl = "Pending";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["complainant"].'</td>
						<td id="complain_'.$data["id"].'">'.$data["complain"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
							<a href="print_blotters.php?brID='.$data["id"].'" class="btn btn-default btn-flat btn-sm" type = "button" target="_blank">
								<i class="fa fa-print"></i>
							</a>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'solved'){
			$status = 'pending';
		}else{
			$status = 'solved';
		}
		$conn-> query("UPDATE `blotters` SET `status`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Status updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$complainant = $conn-> real_escape_string($_POST['complainant']);
	$complain = $conn-> real_escape_string($_POST['complain']);
	$respondent = $conn-> real_escape_string($_POST['respondent']);
	$doc = $conn-> real_escape_string($_POST['doc']);
	$doi = $conn-> real_escape_string($_POST['doi']);
	$poi = $conn-> real_escape_string($_POST['poi']);
	$oi = $conn-> real_escape_string($_POST['oi']);
	
	// Delete data
	if($_POST['key'] == 'deleteRow') {
		$conn -> query ("Delete from holidays where id='$rowID'");
		exit("holidays Deleted!");
	} 
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `blotters` SET `complainant`='$complainant',`complain`='$complain',`details_of_complain`='$doc',`respondent`='$respondent',`place_of_incident`='$poi',`date_of_incident`='$doi',`officer_incharge`='$oi',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from blotters where complainant = '$complainant' and respondent = '$respondent' and date_of_incident = '$doi'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `blotters`(`complainant`, `complain`, `details_of_complain`, `respondent`, `place_of_incident`, `date_of_incident`, `officer_incharge`, `status`, `created_at`, `updated_at`) VALUES ('$complainant','$complain','$doc','$respondent','$poi','$doi','$oi','pending','$dateNow','$dateNow')");
			exit('Data added');
		}
	}
}
?>