<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT first_name, last_name, middle_name, voter_id from residents where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'middle_name' => $data['middle_name'],
			'voter_id' => $data['voter_id']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id as id, CONCAT(last_name, ', ', first_name) AS name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From residents Where registered_voter = 'yes' ORDER BY name DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["active"] == 'active'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" voter_id="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" voter_id = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" voter_id = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id as id, CONCAT(last_name, ', ', first_name) AS name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From residents Where registered_voter = 'yes' ORDER BY name DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["active"] == 'active'){
					$status = "success";
					$statusLbl = "Active";
				}else{
					$status = "danger";
					$statusLbl = "Inactive";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["name"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" voter_id="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" voter_id = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" voter_id = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'active'){
			$status = 'inactive';
		}else{
			$status = 'active';
		}
		$conn-> query("UPDATE `residents` SET `active`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Active updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$voter_id = $conn-> real_escape_string($_POST['voter_id']);
	
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `residents` SET `voter_id`='$voter_id',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
}

?>