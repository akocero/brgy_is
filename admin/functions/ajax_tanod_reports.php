<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from tanod_reports where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array(
			'tanod_officer' => $data['tanod_officer'],
			'details' => $data['details'],
			'date' => $data['date'],
			'place' => $data['place']
		);
		exit(json_encode($jsonArray)); 

	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id, tanod_officer, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, status From tanod_reports ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["status"] == 'solved'){
				$status = "success";
				$statusLbl = "Solved";
			}else{
				$status = "warning";
				$statusLbl = "Pending";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["tanod_officer"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
							<a href="print_tanod_reports.php?trID='.$data["id"].'" class="btn btn-default btn-flat btn-sm" type = "button" target="_blank">
								<i class="fa fa-print"></i>
							</a>
						</td>
					</tr>
				';
				exit($response); 
		}
	}
	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id, tanod_officer, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, status From tanod_reports ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["status"] == 'solved'){
					$status = "success";
					$statusLbl = "Solved";
				}else{
					$status = "warning";
					$statusLbl = "Pending";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.$data["tanod_officer"].'</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["status"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
							<a href="print_tanod_reports.php?trID='.$data["id"].'" class="btn btn-default btn-flat btn-sm" type = "button" target="_blank">
								<i class="fa fa-print"></i>
							</a>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'solved'){
			$status = 'pending';
		}else{
			$status = 'solved';
		}
		$conn-> query("UPDATE `tanod_reports` SET `status`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Status updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$tanod_officer = $conn-> real_escape_string($_POST['tanod_officer']);
	$details = $conn-> real_escape_string($_POST['details']);
	$date = $conn-> real_escape_string($_POST['date']);
	$place = $conn-> real_escape_string($_POST['place']);
	
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `tanod_reports` SET`tanod_officer`='$tanod_officer',`details`='$details',`date`='$date',`place`='$place',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from tanod_reports where tanod_officer = '$tanod_officer' and details = '$details' and date = '$date'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `tanod_reports`(`tanod_officer`, `details`, `date`, `place`, `status`, `updated_at`, `created_at`) VALUES ('$tanod_officer','$details','$date','$place','pending','$dateNow','$dateNow')");
			exit('Data added');
		}
	}
}
?>