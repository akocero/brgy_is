<?php
if (isset($_POST['key'])) {
	// db connection
	include '../../conn.php';
	date_default_timezone_set('Asia/Manila');
	$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'getRowData') {
		$rowID = $conn-> real_escape_string($_POST['rowID']);
		$sql = $conn-> query("SELECT * from residents where id = '$rowID'");
		$data = $sql-> fetch_array();
		$jsonArray = array( 
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'middle_name' => $data['middle_name'],
			'address' => $data['address'],
			'date_of_birth' => $data['date_of_birth'],
			'contact' => $data['contact'],
			'email' => $data['email'],
			'special' => $data['special'],
			'gender' => $data['gender'],
			'status' => $data['status'],
			'updated_at' => $data['updated_at'],
			'pres_id' => $data['pres_id'],
			'pres_id_no' => $data['pres_id_no'],
			'occupation' => $data['occupation'],
			'pob' => $data['place_of_birth'],
			'citizenship' => $data['citizenship'],
			'voter_id' => $data['voter_id'],
			'voter' => $data['registered_voter']
		);

		$sql = $conn->query("SELECT hm.household_id, h.name household_name, h.details household_details From household_members hm, households h Where resident_id = '$rowID' and hm.household_id = h.id");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			$hArray = array( 
					'household_name' => $data['household_name'],
					'household_details' => $data['household_details']
			);
		}else{
			$hArray = array( 
					'household_name' => 'No Household Found',
					'household_details' => 'No Details Found'
			);
		}
		$array = array_merge($jsonArray, $hArray);
		exit(json_encode($array)); 
	}

	if ($_POST['key'] == 'addRow') {
		$response = $status = $statusLbl = "";
		$sql = $conn-> query("SELECT id as id, last_name , first_name, middle_name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From residents ORDER BY id DESC LIMIT 1");
		if($sql->num_rows > 0){
			$data = $sql-> fetch_array();
			if($data["active"] == 'active'){
				$status = "success";
				$statusLbl = "Active";
			}else{
				$status = "danger";
				$statusLbl = "Inactive";
			}
			$response = '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.ucfirst($data["last_name"]).', '.ucfirst($data["first_name"]).' '.ucfirst($data["middle_name"]).'.</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewOReditImg('.$data["id"].')">
								<i class="far fa-image"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
				exit($response); 
		}
	}

	// Data to Datatable
	if ($_POST['key'] == 'getExistingData') {
		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
		$response = $status = $statusLbl = "";
		$sql = $conn->query("SELECT id as id, last_name , first_name, middle_name, Date_format(created_at, '%a, %M %d %Y - %h:%i %p') as date_added, active From residents ORDER BY id DESC LIMIT $start, $limit" );
		if ($sql->num_rows > 0) {
			$response = "";
			while($data = $sql-> fetch_array()) {
				if($data["active"] == 'active'){
					$status = "success";
					$statusLbl = "Active";
				}else{
					$status = "danger";
					$statusLbl = "Inactive";
				}
				$response .= '
					<tr>
						<td>'.$data["id"].'</td>
						<td id="name_'.$data["id"].'">'.ucfirst($data["last_name"]).', '.ucfirst($data["first_name"]).' '.ucfirst($data["middle_name"]).'.</td>
						<td id="status_'.$data["id"].'">
							<button onclick="updateRowStatus('.$data["id"].',\''.$data["active"].'\')"class="btn btn-'.$status.' btn-flat btn-sm" type="button">
							'.$statusLbl.'
							</button>
						</td>
						<td>'.$data["date_added"].'</td>
						<td>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewOReditImg('.$data["id"].')">
								<i class="far fa-image"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'edit\')">
								<i class="far fa-edit"></i>
							</button>
							<button class="btn btn-default btn-flat btn-sm" type = "button" onclick="viewORedit('.$data["id"].',\'view\')">
								<i class="far fa-folder-open"></i>
							</button>
						</td>
					</tr>
				';
			}
			exit($response);
		} else {
			exit ('reachedMax');
		}
	}

	$rowID = $conn-> real_escape_string($_POST['rowID']);
	if ($_POST['key'] == 'renderHouseholdMembers') {

		$sql = $conn->query("SELECT household_id From household_members Where resident_id = '$rowID'");
		if($sql->num_rows > 0){
			$data = $sql->fetch_array();
			$households_id = $data["household_id"];
			$response = "";
			$sql = $conn->query("SELECT h.household_status as status, h.relation as relation, h.resident_id as id, r.first_name as first_name, r.last_name as last_name, r.middle_name as middle_name From household_members h, residents r where household_id = '$households_id' and r.id = h.resident_id");
			if($sql->num_rows > 0){
				while($data = $sql->fetch_array()){
					$response .= '
						<tr>
							<td>'.$data["id"].'</td>
							<td id="name_'.$data["id"].'">'.ucfirst($data["last_name"]).', '.ucfirst($data["first_name"]).' '.ucfirst($data["middle_name"]).'.</td>
							<td>'.$data["relation"].'</td>
							<td>'.$data["status"].'</td>
						</tr>
					';
				}
				exit($response);
			}
		}else{
			exit('<tr ><td colspan="4"><center>No Data found!</center></td></tr>');
		}
		
	}

	if ($_POST['key'] == 'updateRowStatus') {
		$status = $conn-> real_escape_string($_POST['status']);
		if($status == 'active'){
			$status = 'inactive';
		}else{
			$status = 'active';
		}
		$conn-> query("UPDATE `residents` SET `active`='$status',`updated_at`='$dateNow' WHERE id = '$rowID'");
		exit('Active updated');
	}

	
	$rowID = $conn-> real_escape_string($_POST['rowID']);
	$first_name = $conn-> real_escape_string($_POST['first_name']);
	$middle_name = $conn-> real_escape_string($_POST['middle_name']);
	$last_name = $conn-> real_escape_string($_POST['last_name']);
	$dob = $conn-> real_escape_string($_POST['dob']);
	$address = $conn-> real_escape_string($_POST['address']);
	$contact = $conn-> real_escape_string($_POST['contact']);
	$gender = $conn-> real_escape_string($_POST['gender']);
	$special = $conn-> real_escape_string($_POST['special']);
	$email = $conn-> real_escape_string($_POST['email']);
	$pres_id = $conn-> real_escape_string($_POST['pres_id']);
	$pres_id_no = $conn-> real_escape_string($_POST['pres_id_no']);
	$voter = $conn-> real_escape_string($_POST['voter']);
	$status = $conn-> real_escape_string($_POST['status']);
	$occupation = $conn-> real_escape_string($_POST['occupation']);
	$voter_id = $conn-> real_escape_string($_POST['voter_id']);
	$citizenship = $conn-> real_escape_string($_POST['citizenship']);
	$pob = $conn-> real_escape_string($_POST['pob']);
	
	// Delete data
	if($_POST['key'] == 'deleteRow') {
		$conn -> query ("Delete from holidays where id='$rowID'");
		exit("holidays Deleted!");
	} 
	// Update data
	if ($_POST['key'] == 'updateRow') {
		$conn-> query("UPDATE `residents` SET `u_id`='test1',`first_name`='$first_name',`middle_name`='$middle_name',`last_name`='$last_name',`address`='$address',`pres_id`='$pres_id',`pres_id_no`='$pres_id_no',`contact`='$contact',`email`='$email',`date_of_birth`='$dob', `updated_at`='$dateNow',`status`='$status',`registered_voter`='$voter',`place_of_birth`='$pob',`occupation`='$occupation',`citizenship`='$citizenship',`voter_id`='$voter_id',`gender`='$gender',`special`='$special' WHERE id = '$rowID'");
		exit('Data updated');
	}
		
	// Add data
	if ($_POST['key'] == 'addNew') {
		$sql = $conn->query("SELECT id from residents where first_name = '$first_name' and last_name = '$last_name' and date_of_birth = '$dob'");
		if ($sql->num_rows > 0) {
			exit("Data already added");
		}else{
			$conn-> query("INSERT INTO `residents`(`u_id`, `first_name`, `middle_name`, `last_name`, `special`, `address`, `pres_id`, `pres_id_no`, `contact`,`email`, `date_of_birth`, `gender`, `created_at`, `updated_at`, `status`, `active`, `registered_voter`,`voter_id`,`occupation`,`citizenship`,`place_of_birth`) VALUES ('test1','$first_name','$middle_name','$last_name','$special','$address','$pres_id','$pres_id_no','$contact','$email','$dob','$gender','$dateNow','$dateNow','$status','active','$voter','$voter_id','$occupation','$citizenship','$pob')");
			exit('Data added');
		}
	}
}

?>