<?php
include("../../conn.php");
date_default_timezone_set('Asia/Manila');
$dateNow = date("Y-m-d H:i:s");

    if(isset($_POST["key"])){
        $accnt_ID = $conn-> real_escape_string($_POST['accountID']);
        
        if($_POST["key"] == 'renderDetails'){
            $sql = $conn->query("SELECT last_name, first_name, type, username From users Where id = '$accnt_ID'");
            if($sql->num_rows > 0) {
                $data = $sql->fetch_array();
                $jsonArray = array( 
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'username' => $data['username'],
                    'type' => $data['type']
                );
            }
            exit(json_encode($jsonArray)); 
        }

        if($_POST["key"] == 'manageDetails'){
            $accnt_password = $conn-> real_escape_string($_POST['accnt_password']);
            $accnt_first_name = $conn-> real_escape_string($_POST['accnt_first_name']);
            $accnt_last_name = $conn-> real_escape_string($_POST['accnt_last_name']);
            $accnt_username = $conn-> real_escape_string($_POST['accnt_username']);
            $sql = $conn->query("SELECT password from users Where id = '$accnt_ID'");
            if($sql->num_rows > 0){
                $data = $sql->fetch_array();
                if($data["password"] == $accnt_password){
                    $conn->query("UPDATE `users` SET `username`='$accnt_username',`first_name`='$accnt_first_name',`last_name`='$accnt_last_name' WHERE id = '$accnt_ID'");
                    exit("Account Updated");
                }else{
                    exit("Password Authentication Failed");
                }
            }
        }

        if($_POST["key"] == 'manageSecurity'){
            $accnt_new_password = $conn-> real_escape_string($_POST['accnt_new_password']);
            $accnt_duplicate_password = $conn-> real_escape_string($_POST['accnt_duplicate_password']);
            $accnt_old_password = $conn-> real_escape_string($_POST['accnt_old_password']);

            $sql = $conn->query("SELECT password from users where id='$accnt_ID'");
            if($sql->num_rows > 0){
                $data = $sql->fetch_array();
                if($data["password"] == $accnt_old_password){
                    $conn->query("UPDATE `users` SET `password`='$accnt_new_password' WHERE id = '$accnt_ID'");
                    exit("Account Updated");
                }else{
                    exit("Password Authentication Failed");
                }
            }
        }
        
    }

?>
