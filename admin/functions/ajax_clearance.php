<?php
if (isset($_POST['key'])) {
	// db connection
	include("../../conn.php");
	date_default_timezone_set('Asia/Manila');
	$dateNow = date("Y-m-d H:i:s");
	// viewing data
	if ($_POST['key'] == 'genInfo') {
		$resident_id = $conn-> real_escape_string($_POST['resident_id']);
		$sql = $conn-> query("SELECT * from residents where id = '$resident_id'");
		$data = $sql-> fetch_array();
		$jsonArray = array( 
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'middle_name' => $data['middle_name'],
			'address' => $data['address'],
			'date_of_birth' => $data['date_of_birth'],
			'status' => $data['status'],
		);
		exit(json_encode($jsonArray)); 
	}
}
?>