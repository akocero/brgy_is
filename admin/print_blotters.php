<?php
include "session_admin.php";
include_once("../conn.php");
include 'templates/tmp_print_header.php';


$blotterID = $_GET["brID"];
$sql = $conn->query("SELECT * From blotters where id = '$blotterID'");
if ($sql->num_rows > 0) {
	$data = $sql->fetch_array();
	$complainant = $data["complainant"];
	$complain = $data["complain"];
	$details_of_complain = $data["details_of_complain"];
	$place_of_incident = $data["place_of_incident"];
	$date_of_incident = $data["date_of_incident"];
	$officer_incharge = $data["officer_incharge"];
	$respondent = $data["respondent"];
}
?>
<div class="content-wrapper">
	<img src="../Images/logo.png" class="watermark" alt="" width="100%">
	<div class="header">
		<img src="../Images/logo.png" alt="">
		<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
		<img src="../Images/marilaoLogo2.png" alt="">
	</div>
	<div class="line">
		<h4>OFFICE OF THE SANGGUNIANG BARANGAY</h4>
	</div>
	<div class="report-content">
		<br>
		<h3><?php echo $date_of_incident ?></h3>
		<br>
		<br>
		<h3><?php echo $place_of_incident ?></h3>
		<br>
		<br>
		<table class="table">
			<tr>
				<td>
					<h3>Complainant: <?php echo $complainant; ?></h3>
				</td>
				<td>
					<h3>Respondent: <?php echo $respondent; ?></h3>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h3><?php echo $complain; ?></h3>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-right: 8rem"><br><br>
					<h3><?php echo $details_of_complain; ?></h3><br><br><br><br>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h3>Officer Incharge: <?php echo $officer_incharge; ?></h3>
				</td>
			</tr>
		</table>
	</div>
</div>
<!-- Bootstrap -->
<script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>