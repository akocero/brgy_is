<?php
	include "session_admin.php";
	include_once("../conn.php");
	include 'templates/tmp_print_header.php';


	$sql = $conn->query("SELECT id from residents where registered_voter = 'yes' and active = 'active'");
	$totalVoters = $sql->num_rows;
	$rooms = $_GET["r"];
	$totalVoters /= $rooms;
	$start = 0;
	for ($i = 0; $i < $rooms; $i ++){
		
		$length = round($totalVoters);
		$roomNumber = $i + 1;
		if($i == $rooms - 1){
			$length += 10000;
		}
?>
		<div class="content-wrapper">
			<div class="header">
				<img src="../Images/marilaoLogo2.png" alt="">
				<h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
				<img src="../Images/logo.png" alt="">
			</div>
			<div class="line">
				<h4><?php echo 'Room #' . $roomNumber ?></h4>
			</div>
			<table class="table table-sm table-hover table-bordered">
			    <thead style="font-weight: 500;">
			        <tr>
			            <td>Full Name</td>
			            <td>Voters ID</td>
			            <td>Signature</td>
			        </tr>
			    </thead>	
			    <tbody>
			    	<?php
			    		
			    		$sql = $conn->query("SELECT last_name, first_name, middle_name, voter_id from residents where registered_voter = 'yes' and active = 'active' order by last_name LIMIT $start, $length");

			    		if($sql->num_rows > 0) {
			    			while ($data = $sql->fetch_array()) {
			    				$start += 1;
			    				echo '<tr>
										<td>'.ucfirst($data["last_name"]).', '.ucfirst($data["first_name"]).' '.ucfirst($data["middle_name"]).'.</td>
										<td>'.$data["voter_id"].'</td>
										<td></td>	
									</tr>
			    				';
			    			}
			    		}
			    	?>

			    </tbody>
			</table>
		</div>
		<?php } ?>
	<!-- Bootstrap -->
    <script src="../plugins2/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>