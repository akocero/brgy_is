<?php
    include "session_admin.php";
    include_once("../conn.php");
    include 'templates/tmp_header.php';
    date_default_timezone_set('Asia/Manila');
    $dateNow = date("Y.m");
?>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-warning elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span style ="margin-top: -20px;"class="brand-text font-weight-light"><strong>Brgy Sta. Rosa 1</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Summary</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="voters.php" class="nav-link">
                                <i class="nav-icon fas fa-fire"></i>
                                <p>
                                    Registered Voters
                                    <span class="right badge badge-danger">Hot</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Menu</li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Data Entry
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview ">
                                <li class="nav-item">
                                    <a href="residents.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Residents</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="households.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Households</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="blotters.php" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Blotters</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="watch_list.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Watch list</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="tanod_reports.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tanod Reports</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Tools
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="users.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="type_of_id.php" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Presented ID</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Barangay Clearance</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Data Entry</a></li>
                                <li class="breadcrumb-item active">Barangay Clearance</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">                
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="container clearanceContainer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Resident Name</label> 
                                                <select class="form-control select2" style="width: 100%;" id="resident_id">
                                                    <option value="">...</option>
                                                    <?php
                                                        $sql = $conn->query("SELECT id, concat(last_name, ' ', first_name) as name From residents");
                                                        if($sql->num_rows > 0) {
                                                            while ($data = $sql->fetch_array()) {
                                                                echo '<option value="'.$data["id"].'">'.$data["name"].'</option>';
                                                            }
                                                        }
                                                    ?>  
                                                </select>         
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Age (Auto)</label> 
                                                <input type="text" class="form-control" id="text_age" readonly>      
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Civil Status (Auto)</label> 
                                                <input type="text" class="form-control" id="text_status" readonly>      
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address (Auto)</label> 
                                                <textarea id="text_address" cols="30" rows="3" readonly class="form-control"></textarea>    
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>This Certification is issued upon his/her request for</label> 
                                                <select id="text_issued_reason" class="form-control">
                                                    <option value="">...</option>
                                                    <option value="Local Employment">Local Employment</option>
                                                    <option value="Overseas Employment">Overseas Employment</option>
                                                    <option value="Loan">Loan</option>
                                                    <option value="TRU Registration">TRU Registration</option>
                                                    <option value="Wiring Permit">Wiring Permit</option>
                                                    <option value="Others">Others</option>
                                                </select>   
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Others Reason</label> 
                                                <input type="text" class="form-control" id="text_other_reason" readonly>  
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" id="printCert"><i class="fa fa-print"></i> Print Certificate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    <?php include 'templates/tmp_footer.php' ?>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2();

            $("#resident_id").on('change', function() {
                let resident_id = $('#resident_id').val();
                generateInfo(resident_id); 
            });

            $("#text_issued_reason").on('change', function() {
                let reason = $("#text_issued_reason").val();
                if(reason == 'Others'){
                     $("#text_other_reason").removeAttr('readonly'); 
                }
            });

            $('#printCert').on('click', function() {
                printCert();
            });
        });

        function printCert(){
            let resident_id = $('#resident_id');
            let age = $('#text_age');
            let reason = $("#text_issued_reason");
            if(reason.val() == 'Others'){
                reason = $("#text_other_reason"); 
            }
            if(isNotEmpty(resident_id) && isNotEmpty(age) && isNotEmpty(reason)) {
                window.open('print_clearance.php?id=' + resident_id.val() + '&age=' + age.val() + '&issuedFor=' + reason.val(),'_blank');
                window.location.reload();
            }

        }

        function generateInfo(resident_id) {
            $.ajax({
                url: 'functions/ajax_clearance.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    key: 'genInfo',
                    resident_id: resident_id
                },
                success: function(response) {
                    $('#text_address').html(response.address);
                    $('#text_status').val(response.status);
                    genAge(response.date_of_birth);
                }
            });
        }

        function genAge(dobVal) {
            const yearNow = <?php echo $dateNow; ?>;
            const dob = dobVal.replace('-','.').substr(0,7);
            const ageVal = yearNow - dob;
            const age = Math.floor(ageVal);
            $('#text_age').val(age);
        }

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.addClass('is-invalid');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.removeClass('is-invalid');
                return true;
            }

        }
    </script>
</body>

</html>