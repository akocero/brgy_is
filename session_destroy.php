<?php
	session_start();

	unset($_SESSION['type']);
	unset($_SESSION['id']);
	session_destroy();
	header('location: index.php');
	exit();
?>