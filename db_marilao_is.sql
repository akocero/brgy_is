-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2021 at 05:37 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_marilao_is`
--

-- --------------------------------------------------------

--
-- Table structure for table `blotters`
--

CREATE TABLE `blotters` (
  `id` int(11) NOT NULL,
  `complainant` varchar(155) NOT NULL,
  `complain` varchar(155) NOT NULL,
  `details_of_complain` varchar(1000) NOT NULL,
  `respondent` varchar(155) NOT NULL,
  `place_of_incident` varchar(155) NOT NULL,
  `date_of_incident` date NOT NULL,
  `officer_incharge` varchar(155) NOT NULL,
  `status` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blotters`
--

INSERT INTO `blotters` (`id`, `complainant`, `complain`, `details_of_complain`, `respondent`, `place_of_incident`, `date_of_incident`, `officer_incharge`, `status`, `created_at`, `updated_at`) VALUES
(1, 'test2', 'Noisy', 'Videoke ', 'mark demetrio', 'Maingay street', '2020-05-08', 'Adonis Mauro', 'solved', '2020-05-08 20:00:49', '2020-06-12 19:02:11'),
(2, 'Chrissandras', 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat aperiam, dolorem tempore facere, repudiandae nesciunt iure. Officia error odio tenetur quos, ab assumenda in ut nisi officiis harum necessitatibus explicabo itaque iure, animi commodi rerum dignissimos quod mollitia tempora, repudiandae qui vel dolore consequuntur labore. Assumenda reprehenderit architecto, distinctio expedita.', 'Lorem ipsum dolor', 'Maingay Street', '2020-05-21', 'Dong Tahimik', 'solved', '2020-05-09 14:57:43', '2020-06-12 19:02:11');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `location` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` varchar(155) NOT NULL,
  `contact_no` varchar(155) NOT NULL,
  `issued_date` date NOT NULL,
  `expiration_period` date NOT NULL,
  `operator` varchar(155) NOT NULL,
  `owner_info` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `name`, `location`, `address`, `status`, `contact_no`, `issued_date`, `expiration_period`, `operator`, `owner_info`, `created_at`, `updated_at`) VALUES
(1, 'madisson\'s', 'blk 6 lot 10, negosyo street', 'blk 6 lot 10, negosyo street', 'solved', '092384785667', '2020-06-17', '2020-06-20', 'ej estaris', 'kathy dela cruz', '2020-06-14 23:57:57', '2020-06-15 21:49:14'),
(2, 'sadasd', 'asdas', 'dasd', 'solved', 'asdasd', '2020-06-10', '2021-05-12', 'asdasd', 'asdasd', '2020-06-15 00:37:25', '2020-06-15 21:10:57'),
(3, 'asdassss', 'asdasd', 'asda', 'solved', 'sdasdasd', '2020-06-10', '2021-05-12', 'sdasdasd', 'asdasd', '2020-06-15 00:38:30', '2020-06-15 21:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `households`
--

CREATE TABLE `households` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `details` text NOT NULL,
  `active` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `households`
--

INSERT INTO `households` (`id`, `name`, `details`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Badato family', 'badato details', 'active', '2020-05-04 18:25:44', '2020-05-04 19:22:49'),
(2, 'Mauro family', 'mauro details', 'active', '2020-05-04 18:25:44', '2020-05-05 15:41:39'),
(4, 'Mariano Family', 'Mariano Family Details', 'active', '2020-05-04 19:08:23', '2020-05-05 13:52:43'),
(5, 'Rodney De leon Jr Family', 'Rodney De Leon  Jr Family Details', 'active', '2020-05-05 13:54:51', '2020-05-19 21:39:08'),
(6, 'Dalisay Cardo Family', 'asdasd', 'active', '2020-05-05 14:08:38', '2020-05-11 12:27:45'),
(7, 'LongBern Family', 'LongBern Family Details', 'active', '2020-05-05 14:10:01', '2020-05-19 21:39:08'),
(8, 'Jay Ricomarc Family', 'Jay Ricomarc Family Details', 'active', '2020-05-05 14:38:39', '2020-05-10 22:27:15'),
(9, 'De larux Chris Family', 'asdfsafasf', 'active', '2020-05-05 14:40:31', '2020-05-11 12:27:25'),
(10, 'Maximillian Saquing Family', 'lng', 'active', '2020-05-05 15:16:02', '2020-05-21 12:34:44'),
(11, 'Eugene Paul Badato Family', 'Eugene Paul Badato Family Details', 'active', '2020-05-05 16:41:35', '2020-05-23 21:31:33'),
(12, 'Otit Family', 'klasdklsad', 'active', '2020-06-12 14:55:51', '2020-06-12 14:55:51'),
(13, 'badato, eugene family', 'asd', 'active', '2020-06-12 14:57:47', '2020-06-12 14:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `household_members`
--

CREATE TABLE `household_members` (
  `id` int(11) NOT NULL,
  `household_id` varchar(155) NOT NULL,
  `relation` varchar(155) NOT NULL,
  `household_status` varchar(155) NOT NULL,
  `resident_id` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `household_members`
--

INSERT INTO `household_members` (`id`, `household_id`, `relation`, `household_status`, `resident_id`, `created_at`, `updated_at`) VALUES
(30, '7', '', 'member', '9', '2020-05-05 15:38:56', '2020-05-05 15:38:56'),
(47, '2', '', 'member', '0', '2020-05-05 16:29:14', '2020-05-05 16:29:14'),
(57, '8', '', 'head', '6', '2020-05-05 16:34:03', '2020-05-05 16:34:03'),
(58, '2', '', 'member', '7', '2020-05-05 16:42:32', '2020-05-05 16:42:32'),
(60, '2', '', 'member', '3', '2020-05-08 18:18:54', '2020-05-08 18:18:54'),
(70, '11', 'head', 'head', '1', '2020-06-03 13:58:12', '2020-06-03 13:58:12'),
(71, '11', 'wife', 'member', '8', '2020-06-04 12:16:37', '2020-06-04 12:16:37'),
(72, '12', 'head', 'head', '22', '2020-06-12 14:56:50', '2020-06-12 14:56:50'),
(74, '12', 'brother', 'member', '2', '2020-06-12 18:48:07', '2020-06-12 18:48:07'),
(75, '1', 'brother', 'member', '11', '2020-06-14 23:36:09', '2020-06-14 23:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `id_type`
--

CREATE TABLE `id_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `udpated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `officials`
--

CREATE TABLE `officials` (
  `id` int(11) NOT NULL,
  `full_name` varchar(155) NOT NULL,
  `position` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `officials`
--

INSERT INTO `officials` (`id`, `full_name`, `position`) VALUES
(1, 'Kenneth R. Delos Reyes', 'chairman'),
(2, 'Councilor Name 1', 'councilor'),
(3, 'Councilor Name 2', 'councilor'),
(4, 'Councilor Name 3', 'councilor'),
(5, 'Councilor Name 4', 'councilor'),
(6, 'Councilor Name 5', 'councilor'),
(7, 'Councilor Name 6', 'councilor'),
(8, 'Councilor Name 7', 'councilor'),
(9, 'SK Chariman Name', 'sk chairman'),
(10, 'Secretary Name', 'secretary');

-- --------------------------------------------------------

--
-- Table structure for table `residents`
--

CREATE TABLE `residents` (
  `id` int(11) NOT NULL,
  `u_id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `special` varchar(155) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `pres_id` varchar(255) NOT NULL,
  `pres_id_no` varchar(155) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(155) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(155) NOT NULL,
  `occupation` varchar(155) NOT NULL,
  `citizenship` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `registered_voter` varchar(155) NOT NULL,
  `voter_id` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `residents`
--

INSERT INTO `residents` (`id`, `u_id`, `first_name`, `middle_name`, `last_name`, `special`, `address`, `gender`, `pres_id`, `pres_id_no`, `contact`, `email`, `date_of_birth`, `place_of_birth`, `occupation`, `citizenship`, `created_at`, `updated_at`, `status`, `active`, `registered_voter`, `voter_id`) VALUES
(1, 'test1', 'eugenes', 'paul', 'badato', 'jr', 'francisco homes', 'male', 'license', '', '09329372733', '', '1995-07-13', 'bulacan', 'Brgy Tanod', 'Filipino', '2020-05-04 14:35:53', '2020-06-04 11:58:43', 'single', 'active', 'yes', '2'),
(2, 'test1', 'Adonis', 'La lang', 'Mauro', '', 'pleasant', 'male', 'umid', '', '121212', '', '1958-05-18', 'bulacan', 'encoder', 'Filipino', '2020-05-04 16:39:17', '2020-06-04 12:03:36', 'single', 'active', 'yes', '2'),
(3, 'test1', 'asd', 'dasd', 'asdas', '', 'asdasd', 'male', 'license', '', 'asdasd', '', '2020-05-11', 'bulacan', 'IT', 'Filipino', '2020-05-04 16:41:28', '2020-05-27 14:40:30', 'single', 'active', 'yes', '2'),
(4, 'test1', 'asdsa', 'dsad', 'das', '', 'asdasd', 'male', 'license', '', 'asdasd', '', '2020-05-28', 'bulacan', 'none', 'Filipino', '2020-05-04 17:00:05', '2020-05-27 14:40:15', 'married', 'active', 'yes', '2'),
(5, 'test1', 'asdasd', 'asd', 'asd', '', 'asdasd', 'female', 'umid', '', 'asdas', '', '2020-05-12', 'bulacan', 'none', 'Filipino', '2020-05-04 17:01:33', '2020-06-03 12:01:20', 'married', 'active', 'yes', '123445645'),
(6, 'test1', 'asdsa', 'sadasd', 'dasd', '', 'asdsad', 'female', 'umid', '', 'asdasd', '', '2020-05-20', 'bulacan', 'none', 'Filipino', '2020-05-04 17:02:40', '2020-05-27 14:39:23', '', 'active', 'yes', '2'),
(7, 'test1', 'Marlon', 'Lespu', 'Masiklat', '', 'asdasd', 'male', 'license', '', 'asdasd', '', '2020-05-11', 'bulacan', 'police', 'Filipino', '2020-05-04 17:03:27', '2020-05-27 14:39:05', 'married', 'active', 'yes', '2'),
(8, 'test1', 'chrissandra', 'nyawskie', 'Vhanguardia', '', 'asdasdasd', 'male', 'umid', '', 'asdasd', '', '2020-05-05', 'bulacan', 'cashier', 'Filipino', '2020-05-04 17:06:10', '2020-05-27 14:38:38', 'married', 'active', 'no', '2'),
(9, 'test1', 'Rodney', 'Chooks', 'Deleon', '', 'asdsadsad', 'male', 'umid', '', 'asdasd', '', '1920-05-19', 'bulacan', 'mis', 'american', '2020-05-04 17:40:35', '2020-05-27 14:38:11', 'married', 'active', 'yes', '78349403'),
(10, 'test1', 'Eubert', 'Malupeng', 'Badato', '', 'sadasd', 'male', 'umid', '', 'asdsad', '', '1995-07-28', 'Canbuluks', 'IT', 'Filipino', '2020-05-04 17:41:21', '2020-05-27 14:37:47', 'married', 'active', 'yes', '12334345456'),
(11, 'test1', 'Eunesis', 'Malupeng', 'Badato', '', 'asdasd', 'male', 'license', '', 'sadsad', '', '2020-05-20', 'bulacan', 'none', 'Filipino', '2020-05-04 17:47:20', '2020-05-27 14:37:26', 'married', 'active', 'no', '2'),
(12, 'test1', 'Ysabelle', 'asd', 'dasd', '', 'sadasd', 'female', 'umid', '', 'sadsad', '', '1970-05-04', 'bulacan', 'IT', 'Filipino', '2020-05-05 19:46:15', '2020-06-03 12:58:08', 'married', 'active', 'yes', '2'),
(13, 'test1', 'Mark Angelo Lorence', 'asdsad', 'Dimagiba', '', 'saddsa', 'male', 'umid', '', 'asdsad', '', '1968-05-04', 'asdsad', 'saddsa', 'saddsa', '2020-05-05 20:49:23', '2020-05-25 00:18:52', 'single', 'active', 'yes', '123213321'),
(14, 'test1', 'sadasd', 'asdsad', 'asd', '', 'sadasd', 'female', 'umid', '', 'asdasd', '', '2020-05-11', 'asd', 'asdasd', 'asd', '2020-05-09 21:30:34', '2020-06-03 12:58:07', 'single', 'active', 'no', 'sadasd'),
(15, 'test1', 'asdsa', 'asd', 'dasd', '', 'asdas', 'female', 'umid', '', 'asdasd', '', '1978-07-06', 'dasdsad', 'sadsa', 'das', '2020-05-09 21:34:02', '2020-05-28 15:34:59', 'married', 'active', 'yes', 'sadsad'),
(16, 'test1', 'asda', 'sada', 'sdsad', '', 'sdsa', 'female', 'license', '', 'asda', '', '2002-05-18', 'dasd', 'asdas', 'dsad', '2020-05-10 23:02:43', '2020-06-03 12:58:07', 'married', 'active', 'yes', 'asdsad'),
(17, 'test1', 'asdsad', 'asdsad', 'asd', '', 'sadsad', 'male', 'license', '', 'asdasd', '', '1998-02-03', 'sadsad', 'sad', 'asd', '2020-05-10 23:03:24', '2020-06-03 12:58:07', 'single', 'active', 'yes', 'asdasd'),
(18, 'test1', 'roy', '', 'lang', '', 'Marilao', 'male', 'license', '', '09292394845', '', '1997-06-23', 'bulacan', 'Brgy Tanod', 'Filipino', '2020-05-22 20:16:13', '2020-05-27 14:36:29', 'married', 'active', 'no', ''),
(19, 'test1', 'Jay', '', 'Guzman', '', 'asdasd', 'female', 'license', '', 'Globe 12312434 /  Smart 22312434 ', 'Test@gmail.com', '2000-02-08', 'asdasd', 'asdasd', 'Filipino', '2020-05-22 20:17:30', '2020-06-12 15:08:19', 'single', 'active', 'yes', '123454545'),
(20, 'test1', 'Sample', 'Samp', 'Sample', 'jr', 'asdasd', 'female', 'umid', '1233', '123123132', 'Test@gmail.com', '2001-06-05', 'dasdas', 'Brgy Tanod', 'Filipino', '2020-05-22 20:27:21', '2020-06-04 12:28:46', 'single', 'active', 'no', ''),
(21, 'test1', 'Ejs', 'Molbif', 'Estaris', 'sr', 'asdasd', 'male', 'license', '', '123123', 'Test@gmail.com', '1990-06-21', 'asdasd', 'asdasd', 'asdasd', '2020-05-23 19:56:56', '2020-06-03 12:58:05', 'single', 'active', 'yes', '23244'),
(22, 'test1', 'asd', '', 'asd', '', 'asd', 'male', 'umid', '', '09292394845', '', '2020-06-17', 'asd', 'asd', 'sad', '2020-06-08 02:49:17', '2020-06-08 02:49:36', 'single', 'active', '', ''),
(23, 'test1', 'asdsa', 'asda', 'dasd', 'jr', 'asdasd', 'male', 'umid', '', '90213', '', '2006-06-06', 'asdasd', 'asdas', 'dasd', '2020-06-14 14:36:26', '2020-06-14 14:36:26', 'married', 'active', '', ''),
(24, 'test1', 'Maxsdasd', '', 'Longbern', 'jr', 'bigkis', 'male', 'license', '', '123123123', '', '1985-06-29', 'Pangasinan', 'driver', 'Filipino', '2020-07-05 14:29:52', '2020-12-13 20:03:16', 'married', 'active', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `resident_picture`
--

CREATE TABLE `resident_picture` (
  `id` int(11) NOT NULL,
  `resident_id` varchar(100) NOT NULL,
  `name` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resident_picture`
--

INSERT INTO `resident_picture` (`id`, `resident_id`, `name`, `created_at`) VALUES
(15, '17', '1592148876resID_17.jpg', '0000-00-00 00:00:00'),
(33, '24', '1610169934resID_24.jpg', '2020-07-05 14:30:28'),
(34, '23', '1610169875resID_23.jpg', '2021-01-09 13:24:35'),
(35, '22', '1616818540resID_22.jpg', '2021-03-27 12:15:40');

-- --------------------------------------------------------

--
-- Table structure for table `tanod_reports`
--

CREATE TABLE `tanod_reports` (
  `id` int(11) NOT NULL,
  `tanod_officer` varchar(155) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(255) NOT NULL,
  `status` varchar(155) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tanod_reports`
--

INSERT INTO `tanod_reports` (`id`, `tanod_officer`, `details`, `date`, `place`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Tanod 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam assumenda ipsum veniam modi dicta perferendis ex placeat quaerat, voluptates. Officiis blanditiis sit obcaecati, et, doloremque quibusdam ex repellendus odit! Dicta!', '2020-05-10', 'Salbahe Street', 'pending', '2021-01-09 13:30:38', '2020-05-10 17:38:51'),
(2, 'Tanod 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam assumenda ipsum veniam modi dicta perferendis ex placeat quaerat, voluptates. Officiis blanditiis sit obcaecati, et, doloremque quibusdam ex repellendus odit! Dicta!', '2020-05-10', 'Maingay Street', 'pending', '2021-01-09 13:30:37', '2020-05-10 17:40:23'),
(3, 'Tanod 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam assumenda ipsum veniam modi dicta perferendis ex placeat quaerat, voluptates. Officiis blanditiis sit obcaecati, et, doloremque quibusdam ex repellendus odit! Dicta!', '2020-05-22', 'asdasds', 'pending', '2021-01-09 13:30:36', '2020-05-10 18:03:27'),
(4, 'asdasd', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam assumenda ipsum veniam modi dicta perferendis ex placeat quaerat, voluptates. Officiis blanditiis sit obcaecati, et, doloremque quibusdam ex repellendus odit! Dicta!', '2020-05-20', 'asdasd', 'pending', '2021-01-09 13:30:35', '2020-05-10 18:10:44'),
(5, 'asdasdsad', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam assumenda ipsum veniam modi dicta perferendis ex placeat quaerat, voluptates. Officiis blanditiis sit obcaecati, et, doloremque quibusdam ex repellendus odit! Dicta!', '2020-05-14', 'asdasd', 'solved', '2021-01-09 13:30:42', '2020-05-10 18:12:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(155) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `security_question` varchar(155) NOT NULL,
  `security_answer` varchar(155) NOT NULL,
  `type` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `security_question`, `security_answer`, `type`, `active`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'Eugene', 'Dev', 'favorite color', 'dark grey', 'admin', 'active', '2020-05-04 13:48:06', '2020-12-13 21:10:55'),
(2, 'chris', 'chris', 'chris', 'van', 'favNumber', 'red', 'admin', 'active', '2020-05-05 19:10:09', '2020-05-10 22:15:57'),
(3, 'eun', 'eun', 'Eunesis', 'Badato', 'favColor', 'red', 'admin', 'active', '2020-05-05 19:17:54', '2020-12-13 21:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `watch_list`
--

CREATE TABLE `watch_list` (
  `id` int(11) NOT NULL,
  `first_name` varchar(155) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `middle_name` varchar(155) NOT NULL,
  `alias` varchar(155) NOT NULL,
  `case_name` varchar(155) NOT NULL,
  `details` text NOT NULL,
  `active` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `watch_list`
--

INSERT INTO `watch_list` (`id`, `first_name`, `last_name`, `middle_name`, `alias`, `case_name`, `details`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Watch list', 'Two', 'Two', 'Two', 'Drug Lord', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae asperiores sit adipisci earum, alias, aliquid, provident dolorum ullam odio nostrum vero magni ipsa quos dolores debitis! Tempora quos rerum, totam!', 'active', '2020-05-09 21:42:34', '2020-07-05 14:34:33'),
(2, 'Jv', 'De Guzman', '', 'Batik', 'Drug Lord', 'Makinis', 'active', '2020-05-09 22:13:33', '2020-07-05 14:34:34');

-- --------------------------------------------------------

--
-- Table structure for table `wl_cases`
--

CREATE TABLE `wl_cases` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `details` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blotters`
--
ALTER TABLE `blotters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `households`
--
ALTER TABLE `households`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `household_members`
--
ALTER TABLE `household_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `id_type`
--
ALTER TABLE `id_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officials`
--
ALTER TABLE `officials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residents`
--
ALTER TABLE `residents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resident_picture`
--
ALTER TABLE `resident_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tanod_reports`
--
ALTER TABLE `tanod_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `watch_list`
--
ALTER TABLE `watch_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_cases`
--
ALTER TABLE `wl_cases`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blotters`
--
ALTER TABLE `blotters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `households`
--
ALTER TABLE `households`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `household_members`
--
ALTER TABLE `household_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `id_type`
--
ALTER TABLE `id_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `officials`
--
ALTER TABLE `officials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `residents`
--
ALTER TABLE `residents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `resident_picture`
--
ALTER TABLE `resident_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tanod_reports`
--
ALTER TABLE `tanod_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `watch_list`
--
ALTER TABLE `watch_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wl_cases`
--
ALTER TABLE `wl_cases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
