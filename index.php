<?php
    session_start();
    if(isset($_SESSION["type"])) {
        header("location: session.php");
    }
    include("conn.php");
    
    if(isset($_POST['login'])) {
        $username = $conn->real_escape_string($_POST['username']);
        $password = $conn->real_escape_string($_POST['password']);

        $sql = $conn->query("SELECT id,type,first_name from users Where username = '$username' and password = '$password' and active = 'active'");   
        if($sql-> num_rows > 0) {
            $data = mysqli_fetch_assoc($sql);
            $_SESSION['type'] = $data["type"];
            $_SESSION['user_id'] = $data["id"];
            $_SESSION['user_first_name'] = $data["first_name"];
            exit('Authentication Success');
        }else{
            exit('Authentication Failed!');
        }
    }

    if(isset($_POST['forgotPassword'])) {

        if($_POST["forgotPassword"] == 'accountDetails'){
            $fp_username = $conn->real_escape_string($_POST['fp_username']);
            $fp_first_name = $conn->real_escape_string($_POST['fp_first_name']);
            $fp_last_name = $conn->real_escape_string($_POST['fp_last_name']);

            $sql = $conn->query("SELECT id,security_question from users where username = '$fp_username' and first_name = '$fp_first_name' and last_name = '$fp_last_name'");   
            if($sql->num_rows > 0) {
                $data = $sql->fetch_array();
                $jsonArray = array(
                    'security_question' => $data['security_question'],
                    'fp_accountID' => $data['id'],
                    'error' => 'No error'
                );
                exit(json_encode($jsonArray)); 
            }else{
                $jsonArray = array(
                    'error' => 'Incorrect details!'
                );
                exit(json_encode($jsonArray));
            }
        }

        if($_POST["forgotPassword"] == 'accountAnswer'){
            $fp_accountID = $conn->real_escape_string($_POST['fp_accountID']);
            $fp_s_answer = $conn->real_escape_string($_POST['fp_s_answer']);

            $sql = $conn->query("SELECT id from users where id = '$fp_accountID' and security_answer = '$fp_s_answer' ");
            if($sql->num_rows > 0){
                exit("Authentication Success");
            }else{
                exit("Authentication Failed");
            }
        }

        if($_POST["forgotPassword"] == 'accountNewPassword'){
            $fp_duplicate_password = $conn->real_escape_string($_POST['fp_duplicate_password']);
            $fp_new_password = $conn->real_escape_string($_POST['fp_new_password']);
            $fp_accountID = $conn->real_escape_string($_POST['fp_accountID']);

            $conn->query("UPDATE `users` SET `password`='$fp_new_password' WHERE id = '$fp_accountID'");
            exit("Authentication Success");
        }
        
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Marilao IS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins2/toastr/toastr.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins2/fontawesome/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins2/adminLTE/dist/css/adminlte.min.css">
    <!-- my style -->
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition login-page">
    <div class="logos">
        <img src="images/logo.png" width="10%">
        <h4>Republic of the Philippines<br>Province of Bulacan<br>Municipality of Marilao<br><strong>Barangay Sta. Rosa 1</strong></h4>
        <img src="images/marilaoLogo2.png" width="10%">
    </div>
    <div class="login-box">
        
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Management Information System</p>
                <form id="formlogin" method="post" action="index.php">
                    <div class="input-group mb-3">
                        <input type="text" id="text_username" class="form-control" placeholder="Username...">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" id="text_password" class="form-control" placeholder="Password...">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12">
                            <a href="#" id="btnForgotPassword">Forgot Password?</a>
                            
                            <button type="submit" class="mt-2 btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
        <div>
            <a href="" class="text-center">
                <p style="opacity: 0.4; color: black;">Developed by: Eugene Paul Badato</p>
            </a>
        </div>
    </div>
    <!-- fp meaning forget password  -->
    <div id="modalPassword" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    Enter your account details
                </div>
                <div class="modal-body">
                    <div class="row" id="fp_username">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Username</label>
                                <input id="text_fp_username" type="text" class="form-control" placeholder="Ex. officer12">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Firstname</label>
                                <input id="text_fp_first_name" type="text" class="form-control" placeholder="Ex. john">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Lastname</label>
                                <input id="text_fp_last_name" type="text" class="form-control" placeholder="Ex. doe">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="fp_security_question">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Your Security Question</label>
                                <select id="text_s_question" class="form-control" disabled>
                                    <option value=""></option>
                                    <option value="favColor">Favorite Color?</option>
                                    <option value="favNumber">Favorite Number?</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Enter your Answer</label>
                                <input id="text_s_answer" type="text" class="form-control" placeholder="Ex. Red">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="fp_new_password">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Enter new password</label>
                                <input id="text_fp_new_password" type="password" class="form-control" placeholder="Ex. officer12">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Enter new password again</label>
                                <input id="text_fp_duplicate_password" type="password" class="form-control" placeholder="Ex. officer12">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="0" id="fp_accountID">
                </div>
                <div class="modal-footer">
                    <input id="btn_fp_username" onclick="" type="button" class="btn btn-success btn-block btn-flat" value="Next"> 
                    <input id="btn_fp_question" onclick="" type="button" class="btn btn-success btn-block btn-flat" value="Next"> 
                    <input id="btn_fp_new_password" onclick="" type="button" class="btn btn-success btn-block btn-flat" value="Next"> 
                </div>
            </div>
        </div>
    </div>
    <!-- /.login-box -->
    
    <!-- jQuery -->
    <script src="plugins2/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins2/bootstrap/js/bootstrap.min.js"></script>
    <!-- Toastr -->
    <script src="plugins2/toastr/toastr.min.js"></script>

    <script type="text/javascript">
        $("#document").ready(function () {
            
            $('#btnForgotPassword').on('click', function(){
                $('#btn_fp_new_password').hide();
                $('#btn_fp_question').hide();
                $('#fp_security_question').hide();
                $('#fp_new_password').hide();
                $('#modalPassword').modal('show');
            });
            $('#btn_fp_username').on('click', function(){
                validateAccountDetails();
            });

            $('#btn_fp_question').on('click', function(){
                validateSecurityAnswer();
            });

            $('#btn_fp_new_password').on('click', function(){
                newPassword();
            });

            $('#text_fp_duplicate_password').on('keyup',function(){
                validateNewPassword();
            });
            $('#text_fp_new_password').on('keyup',function(){
                validateNewPassword();     
            });

            $("#formlogin").submit( function(e) {
                e.preventDefault();
                var password = $("#text_password").val();
                var username = $("#text_username").val();

                if(username == "" || password == "") {
                    toastr.warning('Please check your inputs!');
                }else{
                    $.ajax({
                        url: 'index.php', 
                        dataType: 'text',
                        method: 'POST',
                        data: {
                            login: 1,
                            username: username,
                            password: password
                        }, success: function (response) {
                            if(response.indexOf('Success') >= 0) {
                                window.location = 'session.php';
                            }else{
                                toastr.error(response);
                            }
                        }
                    });
                }
            });
        });

        function validateAccountDetails(){
            var fp_username = $("#text_fp_username").val();
            var fp_first_name = $("#text_fp_first_name").val();
            var fp_last_name = $("#text_fp_last_name").val();

            if(fp_username == "" || fp_first_name == "" || fp_last_name == "") {
                toastr.warning('Please check your inputs!');
            }else{
                $.ajax({
                    url: 'index.php', 
                    dataType: 'json',
                    method: 'POST',
                    data: {
                        forgotPassword: 'accountDetails',
                        fp_username: fp_username,
                        fp_first_name: fp_first_name,
                        fp_last_name: fp_last_name
                    }, success: function (response) {
                        var error = response.error;
                        if(error.indexOf('Incorrect') >= 0){
                            toastr.error(error);
                        }else{
                            $('#text_s_question').val(response.security_question);
                            $('#fp_accountID').val(response.fp_accountID);
                            $('#fp_username').hide();
                            $('#fp_security_question').show();
                            $('#fp_new_password').hide();
                            $('#btn_fp_username').hide();
                            $('#btn_fp_question').show();  
                        }
                        
                    }
                });
            }
        }

        function validateSecurityAnswer(){
            var fp_s_answer = $("#text_s_answer").val();
            var fp_accountID = $("#fp_accountID").val();

            if(fp_s_answer == "") {
                toastr.warning('Please check your inputs!');
            }else{
                $.ajax({
                    url: 'index.php', 
                    dataType: 'text',
                    method: 'POST',
                    data: {
                        forgotPassword: 'accountAnswer',
                        fp_accountID: fp_accountID,
                        fp_s_answer: fp_s_answer
                    }, success: function (response) {
                        if(response.indexOf('Success') >= 0) {
                            $('#fp_username').hide();
                            $('#fp_security_question').hide();
                            $('#fp_new_password').show();
                            $('#btn_fp_username').hide();
                            $('#btn_fp_question').hide(); 
                            $('#btn_fp_new_password').show();             
                        }else{
                            toastr.error(response);
                        }
                    }
                });
            }
        }

        function newPassword(){
            var fp_new_password = $("#text_fp_new_password").val();
            var fp_duplicate_password = $("#text_fp_duplicate_password").val();
            var fp_accountID = $("#fp_accountID").val();

            if(fp_new_password == "" || fp_duplicate_password == ""){
                toastr.warning('Please check your inputs!');
            }else{
                $.ajax({
                    url: 'index.php', 
                    dataType: 'text',
                    method: 'POST',
                    data: {
                        forgotPassword: 'accountNewPassword',
                        fp_accountID: fp_accountID,
                        fp_duplicate_password: fp_duplicate_password,
                        fp_new_password: fp_new_password
                    }, success: function (response) {
                        if(response.indexOf('Success') >= 0) {
                            toastr.success('Password Changed Succesfully'); 
                            $('#modalPassword').modal('hide');
                            setTimeout(() => {
                                window.location.reload();
                            }, 2000);          
                        }else{
                            toastr.error(response);
                        }
                    }
                });
            }
        }

        function validateNewPassword(){
            var fp_duplicate_password = $('#text_fp_duplicate_password');
            var fp_new_password = $('#text_fp_new_password');

            if(fp_duplicate_password.val() != fp_new_password.val()){
                fp_duplicate_password.addClass('is-invalid');
                fp_new_password.addClass('is-invalid');
            }else{
                fp_duplicate_password.removeClass('is-invalid');
                fp_duplicate_password.addClass('is-valid');
                fp_new_password.removeClass('is-invalid');
                fp_new_password.addClass('is-valid');
            }
        }

        function isNotEmpty(caller) {
            if (caller.val() == '') {
                caller.addClass('is-invalid');
                toastr.error("Please check your inputs!");
                return false;
            } else {
                caller.removeClass('is-invalid');
                return true;
            }
        }

    </script>


</body>
</html>